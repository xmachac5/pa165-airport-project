# PA165 - Airport Manager Microservice Application

## Assignment

Create an information system managing flight records at an airport. The system should allow the users to enter records
about stewards, airplanes and destinations. It should also be possible to update and delete these records. A destination
should contain at least the information about the location of an airport (country, city). Airplane details should
contain the capacity of the plane and its name (and possibly its type as well). A steward is described by his first and
last name. The system should also allow us to record a flight. When recording a flight, it is necessary to set the
departure and arrival times, the origin, the destination and the plane. The system should also check that the plane does
not have another flight scheduled during the time of the this flight. It should also be possible to assign (remove)
stewards to (from) a flight while checking for the steward's availability. The ultimate goal is to have a system capable
of listing flights ordered by date and displaying the flight details (origin, destination, departure time, arrival time,
plane, list of stewards)

## Table of Contents

+ [Technologies Used](#technologies-used)
+ [Modules](#modules)
+ [Visuals](#visuals)
+ [Running the app](#running-the-app)
+ [Health and Observation](#health-and-observation)
+ [Scenarios](#scenarios)
+ [Used ports summarized](#used-ports-summarized)
+ [Contributors](#contributors)

## Technologies Used

+ Java 17
+ Spring Boot (v3.0.4)
+ Tomcat
+ Maven
+ Hibernate ORM
+ Swagger
+ Locust
+ Podman

## Visuals

![Alt text](/diagrams/DTO-Diagram.jpg "DTO class diagram")
![Alt text](/diagrams/UseCase-Diagram.jpg "use case diagram")

## Modules

Airport Manager application consists of four runnable Spring Boot modules. All modules communicate with other modules
through the REST.

### Core

+ The Core is the main module of the project. It contains the essential features of the project.
+ port 8080

### Report

+ The Report module generates pdf documents of flights, airplanes, and airports.
+ port 8085

### User

+ The User module is used for storing information about system users.
+ port 8083

### Weather

+ The weather module is used to get information about the weather from the third-party web application.
+ port 8088


## Running the app

### Run the app using Maven

In the application root directory

```
mvn clean install
```

To run the app on localhost
In each microservice separately

```
 mvn spring-boot:run
```

### Run the using Maven and Podman

### To build with Maven

In the application root directory

```
mvn clean install
```

### Optional: Force images to rebuild

+ This deletes all existing images.

```
podman image list | grep "^localhost/pa165-airport-project_" | awk '{print $3}' | xargs podman image rm

```

### To run the application (all microservices)

In the application root directory

```
podman-compose up
```

### To stop the application (all microservices)

In the application root directory

```
podman-compose down
```

## Health and Observation

### Prometheus

The prometheus collects the metrics from all 4 microservices. The UI runs at the http://localhost:9090.

### Grafana

Grafana runs at http://localhost:3000. To Log in, use the username `admin` and the password `admin`.
There is already one minimalistic dashboard imported. However, feel free to experiment and visualize another attributes.

## Scenarios

+ port 8089

### Scenarios description

### Data creation

#### Firstly, the system manager creates the records in the core module - manager creates these types of records:

- airplane type
- an airplane with created airplane type
- country
- 2 cities
- assigns a country to cities
- 2 airports
- unspecified number of stewards
- unspecified number of flights with a created airplane
- assigns steward to flight
- assigns cities to flight

### Data retrieval

#### Secondly, the basic user gets created data and reports - basic user gets these data and reports:

- all airplanes in the system
- all stewards in the system
- all airports in the system
- all flights in the system
- report of the airplane with id 1
- report of the airport with id 1
- report of flight of id 1

### To run scenarios

Make sure the app is running with `podman-compose up`. Then in the application root directory

+ login to http://localhost:8081


```
cd locust-scenarios
```

```
locust Admin BasicUser
```

+ Open internet browser on http://localhost:8089 and set the options accordingly.

- Number of users 2
- Spawn rate 1
- Host http://localhost

## Used ports summarized

+ User module - port 8083
+ Core module - port 8080
+ Weather module - port 8088
+ Report module - port 8085
+ Locust scenarios - port 8089
+ Prometheus - port 9090
+ Grafana - port 3000

## Contributors

+ Adam Krídl [xkridl](https://gitlab.fi.muni.cz/xkridl)
+ Martin Slovík [xslovik](https://gitlab.fi.muni.cz/xslovik)
+ Matej Hrica [xhrica](https://gitlab.fi.muni.cz/xhrica)
+ Ján Macháček [xmachac5](https://gitlab.fi.muni.cz/xmachac5)

