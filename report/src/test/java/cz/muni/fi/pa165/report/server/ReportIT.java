package cz.muni.fi.pa165.report.server;


import cz.muni.fi.pa165.report.server.service.ReportDocumentService;
import cz.muni.fi.pa165.report.server.service.ReportDocumentServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.assertj.core.api.Assertions.assertThat;


/**
 * Integration tests. Run by "maven verify".
 */
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class ReportIT {

    private static final Logger log = LoggerFactory.getLogger(ReportIT.class);

    ReportDocumentService reportDocumentService = new ReportDocumentServiceImpl();

    Map<String, List<String>> documentData = new HashMap<>();

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void getReportFlightByIdTest(){
        log.debug("getReportFlightByIdTest() running");
        List<String> flightData = new ArrayList<>();
        List<String> stewardsData;
        List<String> airplaneData = new ArrayList<>();
        List<String> airportsData = new ArrayList<>();

        flightData.add("Arrival time of flight is 23:00");
        flightData.add("Departure time of flight is 17:00");
        documentData.put("flight", flightData);

        stewardsData = List.of(new String[]{"Adam Krídl", "Matej Hrica", "Martin Slovík", "Ján Macháček"});
        documentData.put("stewards", stewardsData);


        airplaneData.add("Airplane name is TestPlane");
        airplaneData.add("Airplane capacity is 2000");

        documentData.put("airplane", airplaneData);

        airportsData.add("Arrival airport is Arrival Test Airport");
        airportsData.add("Departure airport is Departure Test Airport");

        documentData.put("airports", airportsData);

        var document = reportDocumentService.reportDocument(documentData, "flight test", ReportType.FLIGHT);

        assertThat(document.toString()).contains("Type/Pages/Count 4");
        assertThat(document.toString()).contains("Flight");
        assertThat(document.toString()).contains("Stewards");
        assertThat(document.toString()).contains("Airplane");
        assertThat(document.toString()).contains("Airports");
        assertThat(document.toString()).contains("flight test");

        log.debug("Document: {}", document);

    }

    @Test
    void getReportAirportByIdTest(){
        log.debug("getReportAirportByIdTest() running");
        List<String> airportData = new ArrayList<>();
        List<String> airportCityData = new ArrayList<>();

        airportData.add("Airport test airport");
        airportData.add("The code of airport is tas");
        airportData.add("Location of airport test airport is 41,2665 12,5994");
        documentData.put("airport", airportData);

        airportCityData.add("Airport is located in the city TestCity");
        airportCityData.add("Country of this city is TestCountry");
        documentData.put("airportCity", airportCityData);

        var document = reportDocumentService.reportDocument(documentData, "airport test", ReportType.AIRPORT);

        assertThat(document.toString()).contains("Type/Pages/Count 2");
        assertThat(document.toString()).contains("Airport");
        assertThat(document.toString()).contains("Airport city");
        assertThat(document.toString()).contains("airport test");

        log.debug("Document: {}", document);

    }

    @Test
    void getReportAirplaneByIdTest(){

        log.debug("getReportAirplaneByIdTest() running");
        List<String> airplaneData = new ArrayList<>();
        List<String> airplaneTypeData = new ArrayList<>();

        airplaneData.add("Airplane name: Test plane");
        airplaneData.add("Airplane capacity: 150");
        documentData.put("airplane", airplaneData);

        airplaneTypeData.add("Airplane type name: Test type");
        documentData.put("airplaneType", airplaneTypeData);

        var document = reportDocumentService.reportDocument(documentData, "airplane test", ReportType.AIRPLANE);

        assertThat(document.toString()).contains("Type/Pages/Count 2");
        assertThat(document.toString()).contains("Airplane");
        assertThat(document.toString()).contains("Airplane type");
        assertThat(document.toString()).contains("airplane test");

        log.debug("Document: {}", document);
    }



}
