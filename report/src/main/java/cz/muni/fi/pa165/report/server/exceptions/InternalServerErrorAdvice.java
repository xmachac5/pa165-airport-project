package cz.muni.fi.pa165.report.server.exceptions;

import cz.muni.fi.pa165.report.server.model.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.OffsetDateTime;

@ControllerAdvice
public class InternalServerErrorAdvice {

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ResponseEntity<ErrorMessage> handleInternalServerErrorException(InternalServerErrorException ex) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setTimestamp(OffsetDateTime.now());
        errorMessage.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        errorMessage.setError(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        errorMessage.setMessage(ex.getMessage());
        errorMessage.setPath(ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString());
        return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
