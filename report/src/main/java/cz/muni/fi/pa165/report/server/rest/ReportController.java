package cz.muni.fi.pa165.report.server.rest;
import cz.muni.fi.pa165.report.server.api.ReportApiDelegate;
import cz.muni.fi.pa165.report.server.facade.ReportFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ReportController implements ReportApiDelegate{
    private final ReportFacade reportFacade;
    @Autowired
    public ReportController(ReportFacade reportFacade) {
        this.reportFacade = reportFacade;
    }
    HttpHeaders headers = new HttpHeaders();
    private HttpHeaders initializeHeaders(){
        headers.clear();
        headers.add("Content-Type", "application/pdf");
        headers.add("Content-Disposition", "attachment; filename=report.pdf");
        return headers;
    }
    @Override
    public ResponseEntity<Resource> getReportFlightById(Long id){

        Resource resource = reportFacade.getReportFlightById(id);

        headers = initializeHeaders();

        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }
    @Override
    public ResponseEntity<Resource> getReportAirportById(Long id){

        Resource resource = reportFacade.getReportAirportById(id);

        headers = initializeHeaders();

        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }
    @Override
    public ResponseEntity<Resource> getReportAirplaneById(Long id){

        Resource resource = reportFacade.getReportAirplaneById(id);

        headers = initializeHeaders();

        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }
}

