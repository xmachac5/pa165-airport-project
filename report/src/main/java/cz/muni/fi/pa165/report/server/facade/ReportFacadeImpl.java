package cz.muni.fi.pa165.report.server.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.muni.fi.pa165.core.client.*;
import cz.muni.fi.pa165.core.client.invoker.ApiClient;
import cz.muni.fi.pa165.core.client.invoker.ApiException;
import cz.muni.fi.pa165.core.client.model.*;
import cz.muni.fi.pa165.report.server.ReportType;
import cz.muni.fi.pa165.report.server.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.report.server.service.ReportDocumentService;
import cz.muni.fi.pa165.user.client.UserApi;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.stereotype.Service;

@Service
public class ReportFacadeImpl implements ReportFacade{

    private final ReportDocumentService reportDocumentService;
    Map<String, List<String>> documentData;

    public ReportFacadeImpl(ReportDocumentService reportDocumentService){
        this.reportDocumentService = reportDocumentService;
        this.documentData = new HashMap<>();
    }

    ApiClient getApiClient() {
        var client = new ApiClient();
        client.setRequestInterceptor((builder -> {
            var authentication = SecurityContextHolder.getContext().getAuthentication();
            var authPrincipal = (OAuth2IntrospectionAuthenticatedPrincipal) authentication.getPrincipal();
            var token = (OAuth2AccessToken) (((BearerTokenAuthentication) authentication).getToken());
            builder.header("Authorization", "Bearer " + token.getTokenValue());
        }));
        return client;
    }

    @Override
    public Resource getReportAirportById(Long id){

        AirportDto airport;
        CityDto cityDto;
        CountryDto countryDto;

        var client = getApiClient();
        CountryApi countryApi = new CountryApi(client);
        CityApi cityApi = new CityApi(client);
        AirportApi airportApi = new AirportApi(client);
        List<String> airportData = new ArrayList<>();
        List<String> cityData = new ArrayList<>();

        //Getting airport data and then calling service creating pdf document from them
        try {
            airport = airportApi.getAirportById(id);

            airportData.add("Airport " + airport.getName());
            airportData.add("The code of airport is " + airport.getCode());
            airportData.add("Location of airport " + airport.getName() + " is: \n"
            + airport.getLocation());
            documentData.put("airport", airportData);

            if(airport.getCityId() != null && airport.getCityId() != 0) {
                cityDto = cityApi.getCityById(airport.getCityId());
                cityData.add("Airport is located in the city " + cityDto.getName());

                if (cityDto.getCountryId() != null && cityDto.getCountryId() != 0){
                    countryDto = countryApi.getCountryById(cityDto.getCountryId());
                    cityData.add("Country of this city is " + countryDto.getName());
                }
            }
            documentData.put("airportCity", cityData);


        } catch (ApiException e) {
            throw new ResourceNotFoundException(e);
        }
        return new ByteArrayResource(reportDocumentService.reportDocument(documentData, "test", ReportType.AIRPORT).toByteArray());
    }

    @Override
    public Resource getReportFlightById(Long id){
        var client = getApiClient();
        FlightApi flightApi = new FlightApi(client);
        StewardApi stewardApi = new StewardApi(client);
        AirplaneApi airplaneApi = new AirplaneApi(client);
        AirportApi airportApi = new AirportApi(client);
        AirplaneDto airplaneDto;
        AirportDto arrivalAirportDto;
        AirportDto departureAirportDto;
        FlightDto flight;
        List<String> flightData = new ArrayList<>();
        List<String> stewardsData = new ArrayList<>();
        List<String> airplaneData = new ArrayList<>();
        List<String> airportsData = new ArrayList<>();

        //Getting flight data and then calling service creating pdf document from them
        try {
            flight = flightApi.getFlightById(id);

            flightData.add("Arrival time of flight is " + flight.getArrivalTime());
            flightData.add("Departure time of flight is " + flight.getDepartureTime());
            documentData.put("flight", flightData);

            if( flight.getAssignedStewardIds() != null){
            flight.getAssignedStewardIds().forEach(stewardId ->
                    {
                        StewardDto stewardDto;
                        try {
                            stewardDto = stewardApi.getSteward(stewardId);
                        } catch (ApiException e) {
                            throw new ResourceNotFoundException(e);
                        }
                        stewardsData.add(stewardDto.getFirstName() + " " + stewardDto.getLastName());
                    });
            }
            documentData.put("stewards", stewardsData);

            if(flight.getAirplaneId() != null && flight.getAirplaneId() != 0) {
                airplaneDto = airplaneApi.getAirplaneById(flight.getAirplaneId());
                airplaneData.add("Airplane name is " + airplaneDto.getName());
                airplaneData.add("Airplane capacity is " + airplaneDto.getCapacity());
            }
            documentData.put("airplane", airplaneData);

            if (flight.getArrivalAirportId() != null && flight.getArrivalAirportId() != 0) {
                arrivalAirportDto = airportApi.getAirportById(flight.getArrivalAirportId());
                airportsData.add("Arrival airport is " + arrivalAirportDto.getName());
            }
            if (flight.getDepartureAirportId() != null && flight.getDepartureAirportId() != 0) {
                departureAirportDto = airportApi.getAirportById(flight.getDepartureAirportId());
                airportsData.add("Departure airport is " + departureAirportDto.getName());
            }
            documentData.put("airports", airportsData);

        } catch (ApiException e) {
            throw new ResourceNotFoundException(e);
        }
        return new ByteArrayResource(reportDocumentService.reportDocument(documentData, "Flight", ReportType.FLIGHT).toByteArray());
    }

    @Override
    public Resource getReportAirplaneById(Long id){
        var client = getApiClient();
        AirplaneApi airplaneApi = new AirplaneApi(client);
        AirplaneTypeApi airplaneTypeApi = new AirplaneTypeApi(client);
        AirplaneTypeDto airplaneTypeDto;
        AirplaneDto airplane;
        List<String> airplaneData = new ArrayList<>();
        List<String> airplaneTypeData = new ArrayList<>();

        //Getting airplane data and then calling service creating pdf document from them
        try {
            airplane = airplaneApi.getAirplaneById(id);

            airplaneData.add("Airplane name: " + airplane.getName());
            airplaneData.add("Airplane capacity: " + airplane.getCapacity());
            documentData.put("airplane", airplaneData);

            airplaneTypeDto = airplaneTypeApi.getAirplaneTypeById(airplane.getTypeId());
            airplaneTypeData.add("Airplane type name: " + airplaneTypeDto.getName());
            documentData.put("airplaneType", airplaneTypeData);

        } catch (ApiException e) {
            throw new ResourceNotFoundException(e);
        }
        return new ByteArrayResource(reportDocumentService.reportDocument(documentData, "Airplane", ReportType.AIRPLANE).toByteArray());
    }
}
