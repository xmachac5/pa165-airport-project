package cz.muni.fi.pa165.report.server;

public enum ReportType {
    AIRPLANE, AIRPORT, FLIGHT
}
