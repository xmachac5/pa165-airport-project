package cz.muni.fi.pa165.report.server.exceptions;

import cz.muni.fi.pa165.report.server.model.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.OffsetDateTime;

@ControllerAdvice
public class ResourceNotFoundAdvice {

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ResponseEntity<ErrorMessage> handleResourceNotFoundException(ResourceNotFoundException ex) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setTimestamp(OffsetDateTime.now());
        errorMessage.setStatus(HttpStatus.NOT_FOUND.value());
        errorMessage.setError(HttpStatus.NOT_FOUND.getReasonPhrase());
        errorMessage.setMessage(ex.getMessage());
        errorMessage.setPath(ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString());
        return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
    }
}
