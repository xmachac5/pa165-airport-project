package cz.muni.fi.pa165.report.server.facade;

import org.springframework.core.io.Resource;

public interface ReportFacade {

    Resource getReportFlightById(Long id);

    Resource getReportAirportById(Long id);

    Resource getReportAirplaneById(Long id);
}
