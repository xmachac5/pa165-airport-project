package cz.muni.fi.pa165.report.server.service;

import com.itextpdf.text.*;
import java.util.List;
import com.itextpdf.text.pdf.PdfWriter;
import cz.muni.fi.pa165.report.server.ReportType;
import cz.muni.fi.pa165.report.server.exceptions.InternalServerErrorException;
import org.springframework.stereotype.Service;
import java.io.ByteArrayOutputStream;
import java.util.Map;

@Service
public class ReportDocumentServiceImpl implements ReportDocumentService{

    private static final Font CATFONT = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static final Font SUBFONT = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);

    @Override
    public ByteArrayOutputStream reportDocument(Map<String, List<String>> data, String name, ReportType reportType) {
        // Creating empty document and according to type calling methods inserting data to it
        com.itextpdf.text.Document document = new com.itextpdf.text.Document(PageSize.A4, 50, 50, 50, 50);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PdfWriter writer;
        try {
            writer = PdfWriter.getInstance(document, out);
        } catch (DocumentException e) {
            throw new InternalServerErrorException(e);
        }
        document.open();
        try {
            addMetaData(document, name, name, name);
            switch (reportType) {
                case AIRPLANE -> airplaneData(document, data);
                case FLIGHT -> flightData(document, data);
                case AIRPORT -> airportData(document, data);
            }
        }
        catch (DocumentException e) {
            throw new InternalServerErrorException(e);
        }
        document.close();
        writer.close();
        return out;
    }

    private static void airplaneData(Document document, Map<String, List<String>> data) throws DocumentException{
        newDataAnchor(document, data.get("airplane"), "Airplane", 1);
        newDataAnchor(document, data.get("airplaneType"), "Airplane type", 2);
    }

    private static void flightData(Document document, Map<String, List<String>> data)
            throws DocumentException{
        newDataAnchor(document, data.get("flight"), "Flight", 1);
        newListAnchor(document, data.get("stewards"), "Stewards", 2);
        newDataAnchor(document, data.get("airplane"), "Airplane", 3);
        newDataAnchor(document, data.get("airports"), "Airports", 4);
    }

    private static void airportData(Document document, Map<String, List<String>> data) throws DocumentException{
        newDataAnchor(document, data.get("airport"), "Airport", 1);
        newDataAnchor(document, data.get("airportCity"), "Airport city", 2);
    }

    private static void createList(Section section, List<String> listData) {
        // Creating pdf document list from data
        com.itextpdf.text.List list = new com.itextpdf.text.List(true, false, 10);
        if (listData != null) {
            listData.forEach(list::add);
        }
        section.add(list);
    }

    private static void addMetaData(Document document, String title, String subject, String keywords) {
        document.addTitle(title);
        document.addSubject(subject);
        document.addKeywords(keywords);
        document.addAuthor("PA165 - Airport Manager");
        document.addCreator("iText");
    }

    private static void addEmptyLines(Paragraph paragraph, int numberOfLines) {
        for (int i = 0; i < numberOfLines; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static void  newDataAnchor (Document document, List<String> data, String name, Integer chapterNumber)
            throws DocumentException{
        Anchor anchor = new Anchor(name, CATFONT);
        anchor.setName(name);

        Chapter chapter = new Chapter(new Paragraph(anchor), chapterNumber);
        Section dataSection= chapter.addSection(new Paragraph(name + " data", SUBFONT));
        if(data != null) {
            data.forEach(value ->
                    dataSection.add(new Paragraph(value)));
        }
        Paragraph paragraph = new Paragraph();
        addEmptyLines(paragraph, 5);
        dataSection.add(paragraph);
        //adding all created components to the document
        document.add(chapter);
    }

    private static void  newListAnchor (Document document, List<String> data, String name, Integer chapterNumber)
            throws DocumentException{
        Anchor anchor = new Anchor(name, CATFONT);
        anchor.setName(name);

        Chapter chapter = new Chapter(new Paragraph(anchor), chapterNumber);
        Section listSection= chapter.addSection(new Paragraph(name + " data", SUBFONT));
        createList(listSection, data);
        Paragraph paragraph = new Paragraph();
        addEmptyLines(paragraph, 5);
        listSection.add(paragraph);
        // now add all this to the document
        document.add(chapter);
    }
}
