package cz.muni.fi.pa165.report.server.service;

import cz.muni.fi.pa165.report.server.ReportType;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

public interface ReportDocumentService {
    ByteArrayOutputStream reportDocument(Map<String, List<String>> data, String name, ReportType reportType);
}
