from locust import HttpUser, task

AUTH_TOKEN = None

def obtain_auth_token_or_crash():
    global AUTH_TOKEN

    import os
    import sys
    import traceback
    from urllib.request import urlopen
    from urllib.error import HTTPError

    ENV_VARIABLE_NAME = "PA165_AIRPORT_AUTH_TOKEN"
    TOKEN_GRABER_URL_HUMAN = "http://localhost:8081"
    TOKEN_GRABER_URL = "http://localhost:8081/api/last-access-token"

    def print_how_to_fix_and_exit():
        print()
        print("To use locust you need to do one of the following:")
        print(f"  A) put an access token into an enviroment variable named {ENV_VARIABLE_NAME}")
        print(f"  B) login at {TOKEN_GRABER_URL_HUMAN} (keep the service running)")
        print(f"     and let locust script grab the token from there")
        print("(if A is specified it takes precedance over B)")
        sys.exit(1)

    AUTH_TOKEN = os.getenv(ENV_VARIABLE_NAME)
    if AUTH_TOKEN is not None:
        print(f"Using the auth token from env variable {ENV_VARIABLE_NAME}")
        return

    try:
        print(f"Trying to obtain auth token from {TOKEN_GRABER_URL}")
        with urlopen(TOKEN_GRABER_URL) as response:
            AUTH_TOKEN = response.read().decode("utf-8")
        print(f"Using token from {TOKEN_GRABER_URL}")
    except HTTPError as e:
            print(f"Failed to grab token (http code {e.code})")
            print(f"Reason: '{e.reason}'")
            try:
                print(f"Error msg: '{e.read().decode('utf-8')}'")
            except Exception:
                print("Could not decode error msg")
                pass
            print_how_to_fix_and_exit()
    except Exception:
        traceback.print_exc()
        print_how_to_fix_and_exit()

obtain_auth_token_or_crash()

class Admin(HttpUser):
    auth_header = {'Authorization': f'Bearer {AUTH_TOKEN}'}
    def on_start(self):

    #create airplane type - just once because creating airplane type more times would report fail due to not unique name
        self.client.post(":8080/api/airplaneTypes", json=
        {
            "name": "Práškovač 2000"
        },
        headers = self.auth_header)

        response  = self.client.get(":8080/api/airplaneTypes/1",
                                    headers = self.auth_header)

        if response.status_code == 200:
            self.client.post(":8080/api/airplanes", json=
            {
                "name": "Prášek",
                "capacity": 2,
                "typeId": 1
            },
            headers = self.auth_header)

        self.client.post(":8080/api/countries", json=
        {
            "name": "Slovensko"
        },
                         headers = self.auth_header)
        self.client.post(":8080/api/cities", json=
        {
            "name": "Holíč"
        },
                         headers = self.auth_header)
        self.client.post(":8080/api/cities", json=
        {
            "name": "Senica"
        },
                         headers = self.auth_header)
        self.client.post(":8080/api/cities/1/countries/1", json=
        {
            "name": "Holíč"
        },
                         headers = self.auth_header)
        self.client.post(":8080/api/cities/2/countries/1", json=
        {
            "name": "Senica"
        },
                         headers = self.auth_header)
        #create 2 airports - just once because airport need to have unique code
        self.client.post(":8080/api/airports", json=
        {
            "name": "Travnik Holic",
            "code": "THL",
            "location": {
                "latitude": 41.40338,
                "longitude": 2.17403
            }
        },
                         headers = self.auth_header)
        self.client.post(":8080/api/airports", json=
        {
            "name": "Hliniste Senica",
            "code": "HSL",
            "location": {
                "latitude": 41.40338,
                "longitude": 2.17403
            }
        },
                         headers = self.auth_header)

    @task
    def create_steward(self):
        self.client.post(":8080/api/stewards", json=
        {
            "firstName": "John",
            "lastName": "Doe"
        },
                         headers = self.auth_header)

    @task
    def create_flight(self):
        self.client.post(":8080/api/flights", json=
        {
            "departureTime": "2023-12-22T12:04:04.493908908+01:00",
            "arrivalTime": "2023-12-22T12:04:04.493908908+01:00",
            "airplaneId": 1
        },
                         headers = self.auth_header)

    @task
    def assign_steward_flight(self):
        response_steward = self.client.get(":8080/api/stewards/1", headers = self.auth_header)
        response_flight = self.client.get(":8080/api/flights/1", headers = self.auth_header)

        if response_steward.status_code == 200 and response_flight.status_code == 200:
            self.client.post(":8080/api/stewards/1/flights/1", headers = self.auth_header)
            self.client.delete(":8080/api/stewards/1/flights/1", headers = self.auth_header)

    @task
    def assign_airport_flight(self):
        response_airport1 = self.client.get(":8080/api/airports/1", headers = self.auth_header)
        response_airport2 = self.client.get(":8080/api/airports/2", headers = self.auth_header)
        response_flight = self.client.get(":8080/api/flights/1", headers = self.auth_header)

        if response_airport1.status_code == 200 and response_flight.status_code == 200 and response_airport2.status_code == 200:
            self.client.post(":8080/api/airports/1/departingFlights/1", headers = self.auth_header)
            self.client.post(":8080/api/airports/2/arrivingFlights/1", headers = self.auth_header)
            self.client.delete(":8080/api/airports/1/departingFlights/1", headers = self.auth_header)
            self.client.delete(":8080/api/airports/2/arrivingFlights/1", headers = self.auth_header)


class BasicUser(HttpUser):
    min_wait = 5000
    max_wait = 15000
    auth_header = {'Authorization': f'Bearer {AUTH_TOKEN}'}

    @task
    def get_airplane(self):
        self.client.get(":8080/api/airplanes", headers = self.auth_header)
    @task
    def get_stewards(self):
        self.client.get(":8080/api/stewards", headers = self.auth_header)
    @task
    def get_airports(self):
        self.client.get(":8080/api/airports", headers = self.auth_header)
    @task
    def get_flights(self):
        self.client.get(":8080/api/flights", headers = self.auth_header)
    @task
    def generate_airplane_report(self):
        self.client.get(":8085/api/reports/airplane/1", headers = self.auth_header)
    @task
    def generate_airport_report(self):
        self.client.get(":8085/api/reports/airport/1", headers = self.auth_header)
    @task
    def generate_flight_report(self):
        self.client.get(":8085/api/reports/flight/1", headers = self.auth_header)
