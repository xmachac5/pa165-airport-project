package cz.muni.pa165.oauth2.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * Spring MVC Controller.
 * Handles HTTP requests by preparing data in model and passing it to Thymeleaf HTML templates.
 */
@Controller
public class MainController {

    private static final Logger log = LoggerFactory.getLogger(MainController.class);

    /**
     * Home page accessible even to non-authenticated users. Displays user personal data.
     */
    @GetMapping("/")
    public String index(Model model, @AuthenticationPrincipal OidcUser user) {
        log.debug("index() user {}", user == null ? "is anonymous" : user.getSubject());

        // user is logged in
        if (user != null) {
            return "redirect:/success";
        }

        return "index";
    }

    private String lastAccessTokenValue = null;

    @GetMapping("/success")
    public String loggedIn(Model model, @AuthenticationPrincipal OidcUser user,
                           @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {
        OAuth2AccessToken accessToken = oauth2Client.getAccessToken();

        log.info("/success :");

        var subject = user.getSubject();
        model.addAttribute("subject", subject);
        log.info("subject: " + subject);

        var scopes = accessToken.getScopes();
        model.addAttribute("scopes", scopes);
        log.info("scopes: " + scopes);

        String accessTokenValue = accessToken.getTokenValue();
        lastAccessTokenValue = accessTokenValue;
        model.addAttribute("accessToken", accessTokenValue);
        log.info("accessToken : " + accessTokenValue);

        return "success";
    }

    @GetMapping("/api/last-access-token")
    public ResponseEntity<String> getToken() {
        if (lastAccessTokenValue == null) {
            return ResponseEntity
                    .status(404)
                    .contentType(MediaType.TEXT_PLAIN)
                    .body("User has not logged in yet.");
        }

        return ResponseEntity
                .status(200)
                .contentType(MediaType.TEXT_PLAIN)
                .body(lastAccessTokenValue);
    }
}