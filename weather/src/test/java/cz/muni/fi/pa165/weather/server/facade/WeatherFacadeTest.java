package cz.muni.fi.pa165.weather.server.facade;

import cz.muni.fi.pa165.weather.server.data.FlightCreationAdvice;
import cz.muni.fi.pa165.weather.server.data.WeatherForecast;
import cz.muni.fi.pa165.weather.server.data.WeatherReason;
import cz.muni.fi.pa165.weather.server.model.FlightCreationAdviceDto;
import cz.muni.fi.pa165.weather.server.model.WeatherForecastDto;
import cz.muni.fi.pa165.weather.server.model.WeatherReasonDto;
import cz.muni.fi.pa165.weather.server.service.WeatherService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SpringBootTest
class WeatherFacadeTest {

    @Autowired
    private WeatherFacade weatherFacade;

    @MockBean
    private WeatherService weatherService;

    private static final WeatherForecast weatherForecast = new WeatherForecast();
    private static final FlightCreationAdvice flightCreationAdvice = new FlightCreationAdvice();

    @BeforeEach
    void setUp() {
        weatherForecast.setTemperature(21.2);
        weatherForecast.setWindSpeed(13);
        weatherForecast.setRain(1.2);
        weatherForecast.setVisibility(13900);

        flightCreationAdvice.setResult(false);
        flightCreationAdvice.setWeatherReason(WeatherReason.NOK_WIND);
    }

    @Test
    void getWeatherForecast() {
        when(weatherService.getWeatherForecast(anyDouble(), anyDouble()))
                .thenReturn(weatherForecast);

        WeatherForecastDto weatherForecastDto = weatherFacade.getWeatherForecast(42.42, -42.42);

        verify(weatherService)
                .getWeatherForecast(42.42, -42.42);
        verifyNoMoreInteractions(weatherService);
        assertThat(weatherForecastDto.getTemperature())
                .isEqualTo(weatherForecast.getTemperature());
        assertThat(weatherForecastDto.getWindSpeed())
                .isEqualTo(weatherForecast.getWindSpeed());
        assertThat(weatherForecastDto.getRain())
                .isEqualTo(weatherForecast.getRain());
        assertThat(weatherForecastDto.getVisibility())
                .isEqualTo(weatherForecast.getVisibility());
    }

    @Test
    void isSafeToCreateFlight() {
        var departureTime = OffsetDateTime.parse("2023-04-17T17:42:00Z");
        var arrivalTime = OffsetDateTime.parse("2023-04-17T21:22:00+02:00");
        when(weatherService.isSafeToCreateFlight(1L, 13L, departureTime, arrivalTime))
                .thenReturn(flightCreationAdvice);

        FlightCreationAdviceDto flightCreationAdviceDto = weatherFacade.isSafeToCreateFlight(
                1L,
                13L,
                departureTime,
                arrivalTime
        );

        verify(weatherService).isSafeToCreateFlight(1L, 13L, departureTime, arrivalTime);
        verifyNoMoreInteractions(weatherService);
        assertFalse(flightCreationAdviceDto.getResult());
        assertThat(flightCreationAdviceDto.getReason())
                .isEqualTo(WeatherReasonDto.NOK_WIND);
    }
}