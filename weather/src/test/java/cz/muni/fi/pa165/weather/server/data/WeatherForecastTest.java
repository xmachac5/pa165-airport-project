package cz.muni.fi.pa165.weather.server.data;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WeatherForecastTest {

    @Autowired
    private WeatherForecast weatherForecast;

    @Test
    void temperatureInSafeBoundsTrue() {
        weatherForecast.setTemperature(21.2);

        assertTrue(weatherForecast.temperatureInSafeBounds());
    }

    @Test
    void temperatureTooHigh() {
        weatherForecast.setTemperature(42.42);

        assertFalse(weatherForecast.temperatureInSafeBounds());
    }

    @Test
    void temperatureTooLow() {
        weatherForecast.setTemperature(-42.42);

        assertFalse(weatherForecast.temperatureInSafeBounds());
    }

    @Test
    void windSpeedInSafeBoundsTrue() {
        weatherForecast.setWindSpeed(13);

        assertTrue(weatherForecast.windSpeedInSafeBounds());
    }

    @Test
    void windSpeedTooHigh() {
        weatherForecast.setWindSpeed(81);

        assertFalse(weatherForecast.windSpeedInSafeBounds());
    }

    @Test
    void rainInSafeBoundsTrue() {
        weatherForecast.setRain(1.2);

        assertTrue(weatherForecast.rainInSafeBounds());
    }

    @Test
    void rainsTooMuch() {
        weatherForecast.setRain(3.2);

        assertFalse(weatherForecast.rainInSafeBounds());
    }

    @Test
    void visibilityInSafeBoundsTrue() {
        weatherForecast.setVisibility(13500);

        assertTrue(weatherForecast.visibilityInSafeBounds());
    }

    @Test
    void visibilityTooLow() {
        weatherForecast.setVisibility(911);

        assertFalse(weatherForecast.visibilityInSafeBounds());
    }
}