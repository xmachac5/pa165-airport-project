package cz.muni.fi.pa165.weather.server.mapper;

import cz.muni.fi.pa165.weather.server.data.WeatherReason;
import cz.muni.fi.pa165.weather.server.model.WeatherReasonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class WeatherReasonMapperTest {

    @Autowired
    private WeatherReasonMapper weatherReasonMapper;

    @Test
    void toDtoWhenOkEverything() {
        var weatherReasonEntity = WeatherReason.OK_EVERYTHING;

        WeatherReasonDto weatherReasonDto = weatherReasonMapper.toDto(weatherReasonEntity);

        assertThat(weatherReasonDto)
                .isEqualTo(WeatherReasonDto.OK_EVERYTHING);
    }

    @Test
    void toDtoWhenNokWind() {
        var weatherReasonEntity = WeatherReason.NOK_WIND;

        WeatherReasonDto weatherReasonDto = weatherReasonMapper.toDto(weatherReasonEntity);

        assertThat(weatherReasonDto)
                .isEqualTo(WeatherReasonDto.NOK_WIND);
    }
}