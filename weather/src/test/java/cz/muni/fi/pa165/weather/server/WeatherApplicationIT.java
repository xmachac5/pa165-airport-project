package cz.muni.fi.pa165.weather.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.weather.server.facade.WeatherFacade;
import cz.muni.fi.pa165.weather.server.model.FlightCreationAdviceDto;
import cz.muni.fi.pa165.weather.server.model.WeatherForecastDto;
import cz.muni.fi.pa165.weather.server.model.WeatherReasonDto;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests. Run by "mvn verify".
 */
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class WeatherApplicationIT {

    private static final Logger log = LoggerFactory.getLogger(WeatherApplicationIT.class);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private WeatherFacade weatherFacade;

    @Test
    void getWeatherForecastTest() throws Exception {
        log.debug("getWeatherForecastTest() running");

        var expectedTemperature = 3.14;
        var expectedWindSpeed = 15.92;
        var expectedRain = 0.8;
        var expectedVisibility = 6000.7;

        when(weatherFacade.getWeatherForecast(4.0, 4.0))
                .thenReturn(new WeatherForecastDto()
                        .temperature(expectedTemperature)
                        .windSpeed(expectedWindSpeed)
                        .rain(expectedRain)
                        .visibility(expectedVisibility)
                );

        var response = mockMvc.perform(get("/api/weatherForecast?latitude=4&longitude=4"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.temperature").value(expectedTemperature))
                .andExpect(jsonPath("$.windSpeed").value(expectedWindSpeed))
                .andExpect(jsonPath("$.rain").value(expectedRain))
                .andExpect(jsonPath("$.visibility").value(expectedVisibility))
                .andReturn().getResponse().getContentAsString();
        log.debug("response: {}", response);

        verify(weatherFacade).getWeatherForecast(4.0, 4.0);
        verifyNoMoreInteractions(weatherFacade);

        var weatherForecastResponse = objectMapper.readValue(response, WeatherForecastDto.class);
        assertThat(weatherForecastResponse.getTemperature()).isEqualTo(expectedTemperature);
        assertThat(weatherForecastResponse.getWindSpeed()).isEqualTo(expectedWindSpeed);
        assertThat(weatherForecastResponse.getRain()).isEqualTo(expectedRain);
        assertThat(weatherForecastResponse.getVisibility()).isEqualTo(expectedVisibility);
    }

    @Test
    void isSafeToCreateFlightTest() throws Exception {
        log.debug("isSafeToCreateFlightTest() running");

        var expectedResult = true;
        var expectedReason = WeatherReasonDto.OK_EVERYTHING;

        String departureTimeAsString = "2012-12-31T22:00:00.000Z";
        String arrivalTimeAsString = "2012-12-31T23:20:00.000Z";
        OffsetDateTime departureTime = OffsetDateTime.parse(departureTimeAsString);
        OffsetDateTime arrivalTime = OffsetDateTime.parse(arrivalTimeAsString);

        when(weatherFacade.isSafeToCreateFlight(
                1L,
                2L,
                departureTime,
                arrivalTime)
        ).thenReturn(new FlightCreationAdviceDto()
                .result(expectedResult)
                .reason(expectedReason)
        );

        var response = mockMvc.perform(get("/api/isSafeToCreateFlight/1/2?departureTime=2012-12-31T22:00:00.000Z&arrivalTime=2012-12-31T23:20:00.000Z"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(expectedResult))
                .andExpect(jsonPath("$.reason").value(expectedReason.getValue()))
                .andReturn().getResponse().getContentAsString();
        log.debug("response: {}", response);

        verify(weatherFacade).isSafeToCreateFlight(1L, 2L, departureTime, arrivalTime);
        verifyNoMoreInteractions(weatherFacade);

        var weatherForecastResponse = objectMapper.readValue(response, FlightCreationAdviceDto.class);
        assertThat(weatherForecastResponse.getResult()).isEqualTo(expectedResult);
        assertThat(weatherForecastResponse.getReason()).isEqualTo(expectedReason);
    }
}