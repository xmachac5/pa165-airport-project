package cz.muni.fi.pa165.weather.server.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class TimeUtilsTest {

    @Test
    void toLocalDateTimeFromUTC() {
        OffsetDateTime offsetDateTime = OffsetDateTime
                .parse("2023-04-17T17:42:18Z");

        LocalDateTime localDateTime = TimeUtils.toLocalDateTime(offsetDateTime);

        assertThat(localDateTime)
                .isEqualTo(LocalDateTime.of(2023, Month.APRIL, 17, 19, 42, 18));
    }

    @Test
    void toLocalDateTimeFromPositiveTimeZone() {
        OffsetDateTime offsetDateTime = OffsetDateTime
                .parse("2014-09-04T21:18:13+01:00");

        LocalDateTime localDateTime = TimeUtils.toLocalDateTime(offsetDateTime);

        assertThat(localDateTime)
                .isEqualTo(LocalDateTime.of(2014, Month.SEPTEMBER, 4, 22, 18, 13));
    }

    @Test
    void toLocalDateTimeFromNegativeZone() {
        OffsetDateTime offsetDateTime = OffsetDateTime
                .parse("2023-05-05T13:09:42-02:00");

        LocalDateTime localDateTime = TimeUtils.toLocalDateTime(offsetDateTime);

        assertThat(localDateTime)
                .isEqualTo(LocalDateTime.of(2023, Month.MAY, 5, 17, 9, 42));
    }

    @Test
    void getHourIndexOf() {
        LocalDateTime localDateTime = LocalDateTime.of(
                2023, Month.APRIL, 17, 14, 42
        );

        int hourIndex = TimeUtils.getHourOf(localDateTime);

        assertThat(hourIndex)
                .isEqualTo(14);
    }
}