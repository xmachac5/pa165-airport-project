package cz.muni.fi.pa165.weather.server.mapper;

import cz.muni.fi.pa165.weather.server.data.FlightCreationAdvice;
import cz.muni.fi.pa165.weather.server.data.WeatherReason;
import cz.muni.fi.pa165.weather.server.model.FlightCreationAdviceDto;
import cz.muni.fi.pa165.weather.server.model.WeatherReasonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
class FlightCreationAdviceMapperTest {

    @Autowired
    private FlightCreationAdviceMapper flightCreationAdviceMapper;

    @Test
    void toDto() {
        var flightCreationAdviceEntity = new FlightCreationAdvice();
        flightCreationAdviceEntity.setResult(false);
        flightCreationAdviceEntity.setWeatherReason(WeatherReason.NOK_RAIN);

        FlightCreationAdviceDto flightCreationAdviceDto = flightCreationAdviceMapper.toDto(flightCreationAdviceEntity);

        assertFalse(flightCreationAdviceDto.getResult());
        assertThat(flightCreationAdviceDto.getReason())
                .isEqualTo(WeatherReasonDto.NOK_RAIN);
    }
}