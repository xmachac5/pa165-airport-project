package cz.muni.fi.pa165.weather.server.mapper;

import cz.muni.fi.pa165.weather.server.data.WeatherForecast;
import cz.muni.fi.pa165.weather.server.model.WeatherForecastDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class WeatherForecastMapperTest {

    @Autowired
    private WeatherForecastMapper weatherForecastMapper;

    @Test
    void toDto() {
        var weatherForecastEntity = new WeatherForecast();
        weatherForecastEntity.setTemperature(23.1);
        weatherForecastEntity.setRain(0.3);
        weatherForecastEntity.setWindSpeed(13.3);
        weatherForecastEntity.setVisibility(18910);

        WeatherForecastDto weatherForecastDto = weatherForecastMapper.toDto(weatherForecastEntity);

        assertThat(weatherForecastDto.getTemperature())
                .isEqualTo(weatherForecastEntity.getTemperature());
        assertThat(weatherForecastDto.getRain())
                .isEqualTo(weatherForecastEntity.getRain());
        assertThat(weatherForecastDto.getWindSpeed())
                .isEqualTo(weatherForecastEntity.getWindSpeed());
        assertThat(weatherForecastDto.getVisibility())
                .isEqualTo(weatherForecastEntity.getVisibility());
    }
}