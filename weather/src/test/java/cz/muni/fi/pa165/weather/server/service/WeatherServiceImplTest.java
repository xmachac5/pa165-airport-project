package cz.muni.fi.pa165.weather.server.service;

import cz.muni.fi.pa165.weather.server.data.HourlyWeatherForecast;
import cz.muni.fi.pa165.weather.server.data.WeatherForecast;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class WeatherServiceImplTest {

    private static final HourlyWeatherForecast hourlyWeatherForecast = new HourlyWeatherForecast();

    @Autowired
    private WeatherServiceImpl weatherService;

    @BeforeEach
    void setUp() {
        hourlyWeatherForecast.setHourly(Map.ofEntries(
                Map.entry("temperature_2m", List.of(14.2, -21.3, 12.8)),
                Map.entry("rain", List.of(0.9, 0.0, 0.1)),
                Map.entry("windspeed_10m", List.of(31.2, 29.1, 42.42)),
                Map.entry("visibility", List.of(12800.0, 13200.0, 5500.0))
        ));
    }

    @Test
    void getWeatherForecastFromHourlyForHour0() {
        WeatherForecast weatherForecast = weatherService
                .getWeatherForecastFromHourlyForHour(hourlyWeatherForecast, 0);

        assertThat(weatherForecast.getTemperature())
                .isEqualTo(14.2);
        assertThat(weatherForecast.getRain())
                .isEqualTo(0.9);
        assertThat(weatherForecast.getWindSpeed())
                .isEqualTo(31.2);
        assertThat(weatherForecast.getVisibility())
                .isEqualTo(12800.0);
    }

    @Test
    void getWeatherForecastFromHourlyForHour1() {
        WeatherForecast weatherForecast = weatherService
                .getWeatherForecastFromHourlyForHour(hourlyWeatherForecast, 1);

        assertThat(weatherForecast.getTemperature())
                .isEqualTo(-21.3);
        assertThat(weatherForecast.getRain())
                .isEqualTo(0.0);
        assertThat(weatherForecast.getWindSpeed())
                .isEqualTo(29.1);
        assertThat(weatherForecast.getVisibility())
                .isEqualTo(13200.0);
    }

    @Test
    void getHourlyForecastUrl() {
        String hourlyForecastUrl = WeatherServiceImpl.getHourlyForecastUrl(42.42, -42.42);

        assertThat(hourlyForecastUrl)
                .isEqualTo("https://api.open-meteo.com/v1/forecast?latitude=42.42&longitude=-42.42&hourly=temperature_2m,rain,visibility,windspeed_10m&forecast_days=1");
    }
}