package cz.muni.fi.pa165.weather.server.mapper;

import cz.muni.fi.pa165.weather.server.data.WeatherReason;
import cz.muni.fi.pa165.weather.server.model.WeatherReasonDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface WeatherReasonMapper {

    WeatherReasonDto toDto(WeatherReason weatherReason);
}
