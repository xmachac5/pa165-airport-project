package cz.muni.fi.pa165.weather.server.service;

import cz.muni.fi.pa165.weather.server.data.FlightCreationAdvice;
import cz.muni.fi.pa165.weather.server.data.WeatherForecast;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public interface WeatherService {

    /**
     * Get the weather info for the given {@code latitude}
     * and {@code longitude} at this moment.
     *
     * @param latitude latitude
     * @param longitude longitude
     * @return weather information
     */
    WeatherForecast getWeatherForecast(
            double latitude,
            double longitude
    );

    /**
     * Find out whether it's safe to create the flight based
     * on {@code departureAirportId}, {@code arrivalAirportId},
     * {@code departureTime} and {@code arrivalTime}. In case of
     * that it isn't safe, returns also the reason why not.
     *
     * @param departureAirportId departure airport's id
     * @param arrivalAirportId arrival airport's id
     * @param departureTime flight departure time
     * @param arrivalTime flight arrival time
     * @return result as written above
     */
    FlightCreationAdvice isSafeToCreateFlight(
            Long departureAirportId,
            Long arrivalAirportId,
            OffsetDateTime departureTime,
            OffsetDateTime arrivalTime
    );
}
