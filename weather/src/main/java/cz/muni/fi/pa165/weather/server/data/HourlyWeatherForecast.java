package cz.muni.fi.pa165.weather.server.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;

/**
 * Entity representation of the data from our Weather API we use,
 * see: <a href="https://open-meteo.com/">open-meteo</a>.
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class HourlyWeatherForecast {

    private double latitude;

    private double longitude;

    @JsonProperty("generationtime_ms")
    private double generationTimeMs;

    @JsonProperty("utc_offset_seconds")
    private long utcOffsetSeconds;

    private String timezone;

    @JsonProperty("timezone_abbreviation")
    private String timezoneAbbreviation;

    private double elevation;

    @JsonProperty("hourly_units")
    private Map<String, String> hourlyUnits;

    private Map<String, List<Object>> hourly;
}
