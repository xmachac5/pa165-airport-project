package cz.muni.fi.pa165.weather.server.mapper;

import cz.muni.fi.pa165.weather.server.data.WeatherForecast;
import cz.muni.fi.pa165.weather.server.model.WeatherForecastDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface WeatherForecastMapper {

    WeatherForecastDto toDto(WeatherForecast weatherForecast);
}
