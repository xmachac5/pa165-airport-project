package cz.muni.fi.pa165.weather.server.mapper;

import cz.muni.fi.pa165.weather.server.data.FlightCreationAdvice;
import cz.muni.fi.pa165.weather.server.model.FlightCreationAdviceDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public abstract class FlightCreationAdviceMapper {

    @Autowired
    protected WeatherReasonMapper weatherReasonMapper;

    @Mapping(target = "reason", expression = "java(weatherReasonMapper.toDto(flightCreationAdvice.getWeatherReason()))")
    public abstract FlightCreationAdviceDto toDto(FlightCreationAdvice flightCreationAdvice);
}
