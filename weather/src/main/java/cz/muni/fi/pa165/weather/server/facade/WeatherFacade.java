package cz.muni.fi.pa165.weather.server.facade;

import cz.muni.fi.pa165.weather.server.model.FlightCreationAdviceDto;
import cz.muni.fi.pa165.weather.server.model.WeatherForecastDto;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public interface WeatherFacade {

    WeatherForecastDto getWeatherForecast(Double latitude, Double longitude);

    FlightCreationAdviceDto isSafeToCreateFlight(
            Long departureAirportId,
            Long arrivalAirportId,
            OffsetDateTime departureTime,
            OffsetDateTime arrivalTime
    );
}
