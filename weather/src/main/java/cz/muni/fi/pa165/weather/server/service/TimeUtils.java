package cz.muni.fi.pa165.weather.server.service;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;

/**
 * Utility class for work with {@link LocalDateTime}.
 */
class TimeUtils {

    private TimeUtils() {
        // Intentionally made private to prevent instantiation.
    }

    static LocalDateTime toLocalDateTime(OffsetDateTime offsetDateTime) {
        return offsetDateTime
                .atZoneSameInstant(ZoneId.of("UTC+02:00"))
                .toLocalDateTime();
    }

    static int getActualHour() {
        return getHourOf(LocalDateTime.now());
    }

    static int getHourOf(LocalDateTime localDateTime) {
        return localDateTime.getHour();
    }
}
