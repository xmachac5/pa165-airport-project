package cz.muni.fi.pa165.weather.server.facade;

import cz.muni.fi.pa165.weather.server.data.FlightCreationAdvice;
import cz.muni.fi.pa165.weather.server.data.WeatherForecast;
import cz.muni.fi.pa165.weather.server.mapper.FlightCreationAdviceMapper;
import cz.muni.fi.pa165.weather.server.mapper.WeatherForecastMapper;
import cz.muni.fi.pa165.weather.server.model.FlightCreationAdviceDto;
import cz.muni.fi.pa165.weather.server.model.WeatherForecastDto;
import cz.muni.fi.pa165.weather.server.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public class WeatherFacadeImpl implements WeatherFacade {

    private final WeatherService weatherService;
    private final WeatherForecastMapper weatherForecastMapper;
    private final FlightCreationAdviceMapper flightCreationAdviceMapper;

    @Autowired
    public WeatherFacadeImpl(
            WeatherService weatherService,
            WeatherForecastMapper weatherForecastMapper,
            FlightCreationAdviceMapper flightCreationAdviceMapper
    ) {
        this.weatherService = weatherService;
        this.weatherForecastMapper = weatherForecastMapper;
        this.flightCreationAdviceMapper = flightCreationAdviceMapper;
    }

    @Override
    public WeatherForecastDto getWeatherForecast(Double latitude, Double longitude) {
        WeatherForecast weatherForecast = weatherService.getWeatherForecast(latitude, longitude);
        return weatherForecastMapper.toDto(weatherForecast);
    }

    @Override
    public FlightCreationAdviceDto isSafeToCreateFlight(
            Long departureAirportId,
            Long arrivalAirportId,
            OffsetDateTime departureTime,
            OffsetDateTime arrivalTime
    ) {
        FlightCreationAdvice flightCreationAdvice = weatherService
                .isSafeToCreateFlight(
                        departureAirportId,
                        arrivalAirportId,
                        departureTime,
                        arrivalTime
                );
        return flightCreationAdviceMapper.toDto(flightCreationAdvice);
    }
}
