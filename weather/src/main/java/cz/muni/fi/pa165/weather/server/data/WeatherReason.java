package cz.muni.fi.pa165.weather.server.data;

public enum WeatherReason {

    OK_EVERYTHING,
    NOK_TEMPERATURE,
    NOK_WIND,
    NOK_RAIN,
    NOK_VISIBILITY
}
