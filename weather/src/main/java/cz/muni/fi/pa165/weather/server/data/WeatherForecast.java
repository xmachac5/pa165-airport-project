package cz.muni.fi.pa165.weather.server.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@RequiredArgsConstructor
public class WeatherForecast {

    private double temperature;

    private double windSpeed;

    private double rain;

    private double visibility;

    private static final double MIN_SAFE_TEMPERATURE = -40.0;

    private static final double MAX_SAFE_TEMPERATURE = 40.0;

    private static final double MAX_SAFE_WINDSPEED = 75;

    private static final double MAX_SAFE_RAIN = 3.0;

    private static final double MIN_SAFE_VISIBILITY = 1000.0;

    public boolean temperatureInSafeBounds() {
        return MIN_SAFE_TEMPERATURE <= temperature &&
                temperature <= MAX_SAFE_TEMPERATURE;
    }

    public boolean windSpeedInSafeBounds() {
        return windSpeed <= MAX_SAFE_WINDSPEED;
    }

    public boolean rainInSafeBounds() {
        return rain <= MAX_SAFE_RAIN;
    }

    public boolean visibilityInSafeBounds() {
        return MIN_SAFE_VISIBILITY <= visibility;
    }
}
