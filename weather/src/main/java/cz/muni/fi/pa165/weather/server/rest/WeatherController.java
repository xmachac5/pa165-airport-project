package cz.muni.fi.pa165.weather.server.rest;

import cz.muni.fi.pa165.weather.server.api.WeatherApiDelegate;
import cz.muni.fi.pa165.weather.server.facade.WeatherFacade;
import cz.muni.fi.pa165.weather.server.model.FlightCreationAdviceDto;
import cz.muni.fi.pa165.weather.server.model.WeatherForecastDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;

@RestController
public class WeatherController implements WeatherApiDelegate {

    private final WeatherFacade weatherFacade;

    @Autowired
    public WeatherController(WeatherFacade weatherFacade) {
        this.weatherFacade = weatherFacade;
    }

    @Override
    public ResponseEntity<WeatherForecastDto> getWeatherForecast(
            Double latitude,
            Double longitude
    ) {
        return ResponseEntity.ok(weatherFacade.getWeatherForecast(latitude, longitude));
    }

    @Override
    public ResponseEntity<FlightCreationAdviceDto> isSafeToCreateFlight(
            Long departureAirportId,
            Long arrivalAirportId,
            OffsetDateTime departureTime,
            OffsetDateTime arrivalTime
    ) {
        return ResponseEntity.ok(
                weatherFacade.isSafeToCreateFlight(
                        departureAirportId,
                        arrivalAirportId,
                        departureTime,
                        arrivalTime
                )
        );
    }
}
