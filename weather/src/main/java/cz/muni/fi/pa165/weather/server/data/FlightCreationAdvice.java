package cz.muni.fi.pa165.weather.server.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class FlightCreationAdvice {

    private boolean result;

    private WeatherReason weatherReason;
}
