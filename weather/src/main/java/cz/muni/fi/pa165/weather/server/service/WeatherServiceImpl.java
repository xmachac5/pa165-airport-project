package cz.muni.fi.pa165.weather.server.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.core.client.AirportApi;
import cz.muni.fi.pa165.core.client.invoker.ApiClient;
import cz.muni.fi.pa165.core.client.invoker.ApiException;
import cz.muni.fi.pa165.core.client.model.AirportDto;
import cz.muni.fi.pa165.weather.server.data.FlightCreationAdvice;
import cz.muni.fi.pa165.weather.server.data.HourlyWeatherForecast;
import cz.muni.fi.pa165.weather.server.data.WeatherForecast;
import cz.muni.fi.pa165.weather.server.data.WeatherReason;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.OffsetDateTime;

@Service
public class WeatherServiceImpl implements WeatherService {

    private final RestTemplate restTemplate = new RestTemplate();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private static final String TEMPERATURE_ATTRIBUTE_NAME = "temperature_2m";
    private static final String RAIN_ATTRIBUTE_NAME = "rain";
    private static final String WIND_ATTRIBUTE_NAME = "windspeed_10m";
    private static final String VISIBILITY_ATTRIBUTE_NAME = "visibility";

    @Override
    public WeatherForecast getWeatherForecast(double latitude, double longitude) {
        String weatherForecastJsonAsString = restTemplate.getForObject(
                getHourlyForecastUrl(latitude, longitude),
                String.class
        );
        try {
            HourlyWeatherForecast hourlyWeatherForecast = objectMapper
                    .readValue(weatherForecastJsonAsString, HourlyWeatherForecast.class);

            return getActualWeatherForecastFromHourlyForecast(hourlyWeatherForecast);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public FlightCreationAdvice isSafeToCreateFlight(
            Long departureAirportId,
            Long arrivalAirportId,
            OffsetDateTime departureTime,
            OffsetDateTime arrivalTime
    ) {
        try {
            var authentication = SecurityContextHolder.getContext().getAuthentication();
            var token = (OAuth2AccessToken) (((BearerTokenAuthentication) authentication).getToken());
            var client = new ApiClient();
            client.setRequestInterceptor((builder -> {
                builder.header("Authorization", "Bearer " + token.getTokenValue());
            }));
            AirportApi airportClient = new AirportApi(client);
            AirportDto departureAirportDto = airportClient.getAirportById(departureAirportId);
            AirportDto arrivalAirportDto = airportClient.getAirportById(arrivalAirportId);

            WeatherForecast departureForecast = getWeatherForecast(
                    departureAirportDto.getLocation().getLatitude(),
                    departureAirportDto.getLocation().getLongitude()
            );
            WeatherForecast arrivalForecast = getWeatherForecast(
                    arrivalAirportDto.getLocation().getLatitude(),
                    arrivalAirportDto.getLocation().getLongitude()
            );

            var flightCreationAdvice = new FlightCreationAdvice();
            flightCreationAdvice.setResult(false);
            if (!departureForecast.temperatureInSafeBounds()
                    || !arrivalForecast.temperatureInSafeBounds()) {
                flightCreationAdvice.setWeatherReason(WeatherReason.NOK_TEMPERATURE);
            } else if (!departureForecast.windSpeedInSafeBounds() ||
                    !arrivalForecast.windSpeedInSafeBounds()) {
                flightCreationAdvice.setWeatherReason(WeatherReason.NOK_WIND);
            } else if (!departureForecast.rainInSafeBounds() ||
                    !arrivalForecast.rainInSafeBounds()) {
                flightCreationAdvice.setWeatherReason(WeatherReason.NOK_RAIN);
            } else if (!departureForecast.visibilityInSafeBounds() ||
                    !arrivalForecast.visibilityInSafeBounds()) {
                flightCreationAdvice.setWeatherReason(WeatherReason.NOK_VISIBILITY);
            } else {
                flightCreationAdvice.setResult(true);
                flightCreationAdvice.setWeatherReason(WeatherReason.OK_EVERYTHING);
            }

            return flightCreationAdvice;
        } catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

    private WeatherForecast getActualWeatherForecastFromHourlyForecast(HourlyWeatherForecast hourlyWeatherForecast) {
        return getWeatherForecastFromHourlyForHour(hourlyWeatherForecast, TimeUtils.getActualHour());
    }

    WeatherForecast getWeatherForecastFromHourlyForHour(
            HourlyWeatherForecast hourlyWeatherForecast,
            int actualHour
    ) {
        var weatherForecast = new WeatherForecast();
        weatherForecast.setTemperature(parseTemperature(hourlyWeatherForecast, actualHour));
        weatherForecast.setRain(parseRain(hourlyWeatherForecast, actualHour));
        weatherForecast.setWindSpeed(parseWindSpeed(hourlyWeatherForecast, actualHour));
        weatherForecast.setVisibility(parseVisibility(hourlyWeatherForecast, actualHour));

        return weatherForecast;
    }


    private double parseValueByAttributeNameAndHour(
            HourlyWeatherForecast hourlyWeatherForecast,
            String attributeName,
            int actualHour
    ) {
        return (double) hourlyWeatherForecast
                .getHourly()
                .get(attributeName)
                .get(actualHour);
    }

    private double parseTemperature(
            HourlyWeatherForecast hourlyWeatherForecast,
            int actualHour
    ) {
        return parseValueByAttributeNameAndHour(
                hourlyWeatherForecast,
                TEMPERATURE_ATTRIBUTE_NAME,
                actualHour
        );
    }

    private double parseRain(
            HourlyWeatherForecast hourlyWeatherForecast,
            int actualHour
    ) {
        return parseValueByAttributeNameAndHour(
                hourlyWeatherForecast,
                RAIN_ATTRIBUTE_NAME,
                actualHour
        );
    }

    private double parseWindSpeed(
            HourlyWeatherForecast hourlyWeatherForecast,
            int actualHour
    ) {
        return parseValueByAttributeNameAndHour(
                hourlyWeatherForecast,
                WIND_ATTRIBUTE_NAME,
                actualHour
        );
    }

    private double parseVisibility(
            HourlyWeatherForecast hourlyWeatherForecast,
            int actualHour
    ) {
        return parseValueByAttributeNameAndHour(
                hourlyWeatherForecast,
                VISIBILITY_ATTRIBUTE_NAME,
                actualHour
        );
    }

    static String getHourlyForecastUrl(
            double latitude,
            double longitude
    ) {
        return "https://api.open-meteo.com/v1/forecast" +
                "?latitude=" + latitude +
                "&longitude=" + longitude +
                "&hourly=" +
                TEMPERATURE_ATTRIBUTE_NAME + "," +
                RAIN_ATTRIBUTE_NAME + "," +
                VISIBILITY_ATTRIBUTE_NAME + "," +
                WIND_ATTRIBUTE_NAME +
                "&forecast_days=1";
    }
}
