package cz.muni.fi.pa165.user.client;

import cz.muni.chat.fi.pa165.user.client.invoker.ApiClient;
import cz.muni.chat.fi.pa165.user.client.invoker.ApiException;

import cz.muni.fi.pa165.user.client.model.NewActionDto;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;


@Component
public class UserServiceRequestInterceptor implements HandlerInterceptor {

    public void sendRequest(HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return;
        }
        var authPrincipal = (OAuth2IntrospectionAuthenticatedPrincipal) authentication.getPrincipal();
        var token = (OAuth2AccessToken) (((BearerTokenAuthentication) authentication).getToken());

        // would lead to infinite recursion
        if (request.getRequestURI().equals("/api/users/action")) {
            return;
        }

        try {
            var client = new ApiClient();
            client.setRequestInterceptor((builder -> {
                builder.header("Authorization", "Bearer " + token.getTokenValue());
            }));
            var api = new UserApi(client);
            api.registerUserAction(new NewActionDto()
                    .url(request.getRequestURI())
                    .httpMethod(request.getMethod())
            );
            request.getRequestURI();
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        sendRequest(request);
        return true;
    }
}