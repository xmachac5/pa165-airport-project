package cz.muni.fi.pa165.user.client;

public class Authorities {
    public static final String MANAGER = "SCOPE_test_1";
    public static final String ADMINISTRATOR = "SCOPE_test_2";
    public static final String AUDITOR = "SCOPE_test_3";
}