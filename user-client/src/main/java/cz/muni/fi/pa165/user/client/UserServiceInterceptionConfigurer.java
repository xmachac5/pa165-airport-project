package cz.muni.fi.pa165.user.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan
public class UserServiceInterceptionConfigurer implements WebMvcConfigurer {

    private final UserServiceRequestInterceptor interceptor;

    @Autowired
    public UserServiceInterceptionConfigurer(UserServiceRequestInterceptor interceptor) {
        this.interceptor = interceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // Add the user interceptor to intercept all requests to your REST endpoints
        registry.addInterceptor(interceptor).addPathPatterns("/api/**");
    }
}
