package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.model.AirportDto;
import cz.muni.fi.pa165.core.model.GPSLocationDto;
import cz.muni.fi.pa165.core.model.NewAirportDtoRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AirportMapperTest {

    @Autowired
    private AirportMapper airportMapper;

    @Test
    void toEntityFromNewRequest() {
        var newAirportDtoRequest = new NewAirportDtoRequest();
        newAirportDtoRequest.setName("Hartsfield–Jackson Atlanta International Airport");
        newAirportDtoRequest.setCode("ATL");
        var gpsLocation = new GPSLocationDto();
        gpsLocation.setLatitude(33.64);
        gpsLocation.setLongitude(-84.42);
        newAirportDtoRequest.setLocation(gpsLocation);

        Airport airport = airportMapper.toEntityFromNewRequest(newAirportDtoRequest);

        assertThat(airport.getName())
                .isEqualTo(newAirportDtoRequest.getName());
        assertThat(airport.getCode())
                .isEqualTo(newAirportDtoRequest.getCode());
        assertThat(airport.getLatitude())
                .isEqualTo(newAirportDtoRequest.getLocation().getLatitude());
        assertThat(airport.getLongitude())
                .isEqualTo(newAirportDtoRequest.getLocation().getLongitude());
    }

    @Test
    void toDto() {
        Airport jacksonAtlantaAirport = new Airport();
        jacksonAtlantaAirport.setId(42L);
        jacksonAtlantaAirport.setName("Hartsfield–Jackson Atlanta International Airport");
        jacksonAtlantaAirport.setCode("ATL");
        var atlanta = new City();
        atlanta.setId(2023L);
        atlanta.setName("Atlanta");
        jacksonAtlantaAirport.setCity(atlanta);
        var jacksonAtlantaAirportLatitude = 33.64;
        var jacksonAtlantaAirportLongitude = -84.41;
        jacksonAtlantaAirport.setLatitude(jacksonAtlantaAirportLatitude);
        jacksonAtlantaAirport.setLongitude(jacksonAtlantaAirportLongitude);
        jacksonAtlantaAirport.setArrivingFlights(createArrivingFlights());
        jacksonAtlantaAirport.setDepartingFlights(createDepartingFlights());

        AirportDto airportDto = airportMapper.toDto(jacksonAtlantaAirport);

        assertThat(airportDto.getId())
                .isEqualTo(jacksonAtlantaAirport.getId());
        assertThat(airportDto.getName())
                .isEqualTo(jacksonAtlantaAirport.getName());
        assertThat(airportDto.getCode())
                .isEqualTo(jacksonAtlantaAirport.getCode());
        assertThat(airportDto.getCityId())
                .isEqualTo(jacksonAtlantaAirport.getCity().getId());
        assertThat(airportDto.getLocation().getLatitude())
                .isEqualTo(jacksonAtlantaAirport.getLatitude());
        assertThat(airportDto.getLocation().getLongitude())
                .isEqualTo(jacksonAtlantaAirport.getLongitude());
        assertThat(airportDto.getArrivingFlightsIds())
                .hasSameSizeAs(jacksonAtlantaAirport.getArrivingFlights());
        for (int i = 0; i < airportDto.getArrivingFlightsIds().size(); i++) {
            assertThat(airportDto.getArrivingFlightsIds().get(i))
                    .isEqualTo(jacksonAtlantaAirport.getArrivingFlights().get(i).getId());
        }
        assertThat(airportDto.getDepartingFlightsIds())
                .hasSameSizeAs(jacksonAtlantaAirport.getDepartingFlights());
        for (int i = 0; i < airportDto.getDepartingFlightsIds().size(); i++) {
            assertThat(airportDto.getDepartingFlightsIds().get(i))
                    .isEqualTo(jacksonAtlantaAirport.getDepartingFlights().get(i).getId());
        }
    }

    @Test
    void getCityId() {
        Long cityId = airportMapper.getCityId(null);

        assertThat(cityId)
                .isZero();
    }

    private List<Flight> createArrivingFlights() {
        var delta700 = new Flight();
        delta700.setId(701L);

        var ke7282 = new Flight();
        ke7282.setId(1L);

        return List.of(delta700, ke7282);
    }

    private List<Flight> createDepartingFlights() {
        var cv6101 = new Flight();
        cv6101.setId(13L);

        var ac7258 = new Flight();
        ac7258.setId(72L);

        var f91551 = new Flight();
        f91551.setId(91L);

        return List.of(cv6101, ac7258, f91551);
    }
}