package cz.muni.fi.pa165.core.service.steward;

import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.data.domain.FlightSteward;
import cz.muni.fi.pa165.core.data.domain.Steward;
import cz.muni.fi.pa165.core.data.repository.flightsteward.FlightStewardRepository;
import cz.muni.fi.pa165.core.data.repository.steward.StewardRepository;
import cz.muni.fi.pa165.core.service.steward.StewardServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.OffsetDateTime;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class StewardServiceImplTests {
    private StewardServiceImpl stewardService;
    private StewardRepository stewardRepository;

    @BeforeEach
    void setUp() {
        stewardRepository = mock(StewardRepository.class);
        FlightStewardRepository flightStewardRepository = mock(FlightStewardRepository.class);
        stewardService = new StewardServiceImpl(stewardRepository,
                flightStewardRepository);
    }

    @Test
    void findByIdTestSuccess() {
        var firstName = "Ondra";
        var lastName = "Nový";
        var steward = new Steward();
        steward.setFirstName(firstName);
        steward.setLastName(lastName);

        when(stewardRepository.findById(steward.getId()))
                .thenReturn(Optional.of(steward));

        var foundStewardByIdOpt = stewardService.findById(steward.getId());

        assertTrue(foundStewardByIdOpt.isPresent());
        assertEquals(foundStewardByIdOpt.get().getFirstName(), firstName);
        assertEquals(foundStewardByIdOpt.get().getLastName(), lastName);
    }

    @Test
    void assignToFlight() {
        var firstName = "Ondra";
        var lastName = "Nový";
        var steward = new Steward();
        steward.setFirstName(firstName);
        steward.setLastName(lastName);

        var flightDepartureTime = OffsetDateTime.now();
        var flightArrivalTime = OffsetDateTime.now();
        var flight = new Flight();
        flight.setArrivalTime(flightArrivalTime);
        flight.setDepartureTime(flightDepartureTime);

        FlightSteward flightSteward = new FlightSteward();

        flightSteward.setFlight(flight);
        flightSteward.setSteward(steward);

        assertEquals(flightSteward.getSteward(), steward);
        assertEquals(flightSteward.getFlight(), flight);
    }


    @Test
    void updateTestSuccess() {
        var stewardToUpdate = new Steward();
        stewardToUpdate.setId(1L);
        var firstName = "Ondra";
        var lastName = "Nový";
        stewardToUpdate.setFirstName(firstName);
        stewardToUpdate.setLastName(lastName);


        var updatedStewardRequest = new Steward();
        var newFirstName = "Jirka";
        var newLastName = "Starý";
        updatedStewardRequest.setFirstName(newFirstName);
        updatedStewardRequest.setLastName(newLastName);

        when(stewardRepository.findById(1L))
                .thenReturn(Optional.of(stewardToUpdate));
        when(stewardRepository.save(stewardToUpdate))
                .thenReturn(updatedStewardRequest);


        assertEquals(firstName, stewardToUpdate.getFirstName());
        assertNotEquals(newFirstName, stewardToUpdate.getFirstName());
        assertEquals(lastName, stewardToUpdate.getLastName());
        assertNotEquals(newLastName, stewardToUpdate.getLastName());

        var afterUpdate = stewardService.update(1L, updatedStewardRequest);

        assertEquals(afterUpdate.getFirstName(), stewardToUpdate.getFirstName());
    }

}

