package cz.muni.fi.pa165.core.service.city;

import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.repository.city.CityRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CityServiceImplTest {

    private CityServiceImpl cityService;
    private CityRepository cityRepository;

    @BeforeEach
    void setUp() {
        cityRepository = mock(CityRepository.class);
        cityService = new CityServiceImpl(cityRepository);
    }

    @Test
    void findByNameTestSuccess() {
        var cityName = "Berlin";
        var city = new City();
        city.setId(1L);
        city.setName(cityName);

        when(cityRepository.findByName(cityName))
                .thenReturn(Optional.of(city));

        var foundCityByNameOpt = cityService.findByName(cityName);

        assertTrue(foundCityByNameOpt.isPresent());
        assertEquals(city.getName(), foundCityByNameOpt.get().getName());
        assertEquals(city, foundCityByNameOpt.get());
    }

    @Test
    void findByNameTestFindsEmpty() {
        var cityName = "Prague";

        when(cityRepository.findByName(cityName))
                .thenReturn(Optional.empty());

        var foundCityByNameOpt = cityService.findByName(cityName);

        assertTrue(foundCityByNameOpt.isEmpty());
    }

    @Test
    void updateTestNotFoundByIdThrowsException() {
        var city = new City();
        city.setId(-1L);
        city.setName("Madrid");

        when(cityService.findById(-1L))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> cityService.update(-1L, city));
    }

    @Test
    void updateTestSuccess() {
        var cityToUpdate = new City();
        cityToUpdate.setId(1L);
        var amsterdam = "Amsterdam";
        cityToUpdate.setName(amsterdam);

        var updatedCityRequest = new City();
        var antwerp = "Antwerp";
        updatedCityRequest.setName(antwerp);

        when(cityRepository.findById(1L))
                .thenReturn(Optional.of(cityToUpdate));
        when(cityRepository.save(cityToUpdate))
                .thenReturn(updatedCityRequest);

        var updatedCountry = cityService.update(1L, updatedCityRequest);

        assertEquals(cityToUpdate.getName(), updatedCountry.getName());
        assertEquals(cityToUpdate, updatedCountry);
        assertEquals(antwerp, cityToUpdate.getName());
        assertNotEquals(amsterdam, cityToUpdate.getName());
    }
}