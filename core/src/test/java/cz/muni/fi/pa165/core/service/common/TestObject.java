package cz.muni.fi.pa165.core.service.common;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;

/**
 * Sample object representing entity for the purpose of tests.
 */
@Entity
public class TestObject extends DomainEntity {

    @Column
    private String description;

    public TestObject() {
    }

    public TestObject(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
