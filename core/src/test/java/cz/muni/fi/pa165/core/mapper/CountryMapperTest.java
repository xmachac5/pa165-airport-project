package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.model.CountryDto;
import cz.muni.fi.pa165.core.model.NewCountryDtoRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CountryMapperTest {

    @Autowired
    private CountryMapper countryMapper;

    @Test
    void toEntityFromNewRequest() {
        var newCountryDtoRequest = new NewCountryDtoRequest();
        newCountryDtoRequest.setName("Canada");

        Country country = countryMapper.toEntityFromNewRequest(newCountryDtoRequest);

        assertThat(country.getName())
                .isEqualTo(newCountryDtoRequest.getName());
    }

    @Test
    void toDto() {
        var country = new Country();
        country.setId(1L);
        country.setName("Canada");
        City toronto = createCity(1L, "Toronto");
        City ottawa = createCity(2L, "Ottawa");
        country.setCities(List.of(toronto, ottawa));

        CountryDto countryDto = countryMapper.toDto(country);

        assertThat(countryDto.getId())
                .isEqualTo(country.getId());
        assertThat(countryDto.getName())
                .isEqualTo(country.getName());
        assertThat(countryDto.getCitiesIds())
                .hasSameSizeAs(country.getCities());
        for (int i = 0; i < countryDto.getCitiesIds().size(); i++) {
            assertThat(countryDto.getCitiesIds().get(i))
                    .isEqualTo(country.getCities().get(i).getId());
        }
    }

    private static City createCity(Long id, String name) {
        var city = new City();
        city.setId(id);
        city.setName(name);
        return city;
    }
}