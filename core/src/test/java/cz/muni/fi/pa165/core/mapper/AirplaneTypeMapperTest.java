package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.model.AirplaneTypeDto;
import cz.muni.fi.pa165.core.model.NewAirplaneTypeDtoRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AirplaneTypeMapperTest {

    @Autowired
    private AirplaneTypeMapper airplaneTypeMapper;

    @Test
    void toEntityFromNewRequest() {
        var newAirplaneTypeDtoRequest = new NewAirplaneTypeDtoRequest();
        newAirplaneTypeDtoRequest.setName("A320");

        AirplaneType airplaneType = airplaneTypeMapper.toEntityFromNewRequest(newAirplaneTypeDtoRequest);

        assertThat(airplaneType.getName())
                .isEqualTo(newAirplaneTypeDtoRequest.getName());
    }

    @Test
    void toDto() {
        var airplaneType = new AirplaneType();
        airplaneType.setId(1L);
        airplaneType.setName("A320");
        airplaneType.setAirplanes(createAirplanes());

        AirplaneTypeDto airplaneTypeDto = airplaneTypeMapper.toDto(airplaneType);

        assertThat(airplaneTypeDto.getId())
                .isEqualTo(airplaneType.getId());
        assertThat(airplaneTypeDto.getName())
                .isEqualTo(airplaneType.getName());
        assertThat(airplaneTypeDto.getAirplanesIds())
                .hasSameSizeAs(airplaneType.getAirplanes());
        for (int i = 0; i < airplaneTypeDto.getAirplanesIds().size(); i++) {
            assertThat(airplaneTypeDto.getAirplanesIds().get(i))
                    .isEqualTo(airplaneType.getAirplanes().get(i).getId());
        }
    }

    @Test
    void getAirplanesTypeId() {
        Long airplaneTypeId = airplaneTypeMapper.getAirplanesTypeId(null);

        assertThat(airplaneTypeId)
                .isZero();
    }

    private List<Airplane> createAirplanes() {
        var a320Airplane = new Airplane();
        a320Airplane.setId(42L);
        a320Airplane.setName("DL123");
        return List.of(a320Airplane);
    }
}