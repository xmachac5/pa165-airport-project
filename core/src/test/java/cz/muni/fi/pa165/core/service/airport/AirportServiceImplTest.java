package cz.muni.fi.pa165.core.service.airport;

import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.repository.airport.AirportRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AirportServiceImplTest {

    private AirportServiceImpl airportService;
    private AirportRepository airportRepository;

    @BeforeEach
    void setUp() {
        airportRepository = mock(AirportRepository.class);
        airportService = new AirportServiceImpl(airportRepository);
    }

    @Test
    void findByNameTestSuccess() {
        var airportName = "Paris Charles de Gaulle";
        var airport = new Airport();
        airport.setName(airportName);

        when(airportRepository.findByName(airportName))
                .thenReturn(Optional.of(airport));

        var foundCityByNameOpt = airportService.findByName(airportName);

        assertTrue(foundCityByNameOpt.isPresent());
        assertEquals(airport.getName(), foundCityByNameOpt.get().getName());
        assertEquals(airport, foundCityByNameOpt.get());
    }

    @Test
    void findByCodeTestSuccess() {
        var airportCode = "JFK";
        var airport = new Airport();
        airport.setCode(airportCode);

        when(airportRepository.findByCode(airportCode))
                .thenReturn(Optional.of(airport));

        var foundCityByCodeOpt = airportService.findByCode(airportCode);

        assertTrue(foundCityByCodeOpt.isPresent());
        assertEquals(airport.getName(), foundCityByCodeOpt.get().getName());
        assertEquals(airport, foundCityByCodeOpt.get());
    }

    @Test
    void findByCityTestSuccess() {
        var city = new City();
        city.setName("Prague");

        var airport = new Airport();
        airport.setCity(city);

        when(airportRepository.findByCity(city))
                .thenReturn(List.of(airport));

        var foundAirportsByCity = airportService.findByCity(city);

        assertFalse(foundAirportsByCity.isEmpty());
        assertEquals(airport.getName(), foundAirportsByCity.get(0).getName());
        assertEquals(airport, foundAirportsByCity.get(0));
    }

    @Test
    void findByCityTestFindsNoAirports() {
        var city = new City();
        city.setName("Namestovo");

        when(airportRepository.findByCity(city))
                .thenReturn(new ArrayList<>());

        var foundAirportsByCity = airportService.findByCity(city);

        assertTrue(foundAirportsByCity.isEmpty());
    }

    @Test
    void updateTestSuccess() {
        var airportToUpdate = new Airport();
        airportToUpdate.setId(1L);
        var heathrow = "Heathrow";
        airportToUpdate.setName(heathrow);
        airportToUpdate.setCity(new City());
        airportToUpdate.getCity().setName("London");
        var lhr = "LHR";
        airportToUpdate.setCode(lhr);

        var updatedAirportRequest = new Airport();
        var stansted = "Stansted";
        updatedAirportRequest.setName(stansted);
        var stn = "STN";
        updatedAirportRequest.setCode(stn);

        when(airportRepository.findById(1L))
                .thenReturn(Optional.of(airportToUpdate));
        when(airportRepository.save(airportToUpdate))
                .thenReturn(updatedAirportRequest);

        var updatedCountry = airportService.update(1L, updatedAirportRequest);

        assertEquals(airportToUpdate.getName(), updatedCountry.getName());
        assertEquals(airportToUpdate, updatedCountry);
        assertEquals(stansted, airportToUpdate.getName());
        assertNotEquals(heathrow, airportToUpdate.getName());
        assertEquals(stn, airportToUpdate.getCode());
        assertNotEquals(lhr, airportToUpdate.getCode());
    }
}