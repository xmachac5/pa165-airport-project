package cz.muni.fi.pa165.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.core.facade.country.CountryFacade;
import cz.muni.fi.pa165.core.model.CountryDto;
import cz.muni.fi.pa165.core.model.NewCountryDtoRequest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests. Run by "maven verify".
 */
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class CoreApplicationIT {

    private static final Logger log = LoggerFactory.getLogger(CoreApplicationIT.class);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private CountryFacade<Long> mockCountryFacade;

    @Test
    void createCountryTest() throws Exception {
        log.debug("createCountryTest() running");

        final long expectedId = 42;
        final String expectedName = "Uganda";

        var request = new NewCountryDtoRequest().name(expectedName);
        var expectedResponse = new CountryDto()
                .id(expectedId)
                .name(expectedName);

        Mockito.when(mockCountryFacade.save(any(NewCountryDtoRequest.class)))
                .thenReturn(expectedResponse);

        var response = mockMvc.perform(
                        post("/api/countries")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(request))
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedId))
                .andExpect(jsonPath("$.name").value(expectedName))
                .andReturn().getResponse().getContentAsString();
        log.debug("response: {}", response);

        var countryDtoResponse = objectMapper.readValue(response, CountryDto.class);
        assertThat(countryDtoResponse.getId()).isEqualTo(expectedId);
        assertThat(countryDtoResponse.getName()).isEqualTo(expectedName);
    }

    @Test
    void getCountryByIdTest() throws Exception {
        log.debug("getCountryByIdTest() running");

        var expectedId = 4242L;
        var expectedName = "Fraaaance";

        var expectedResponse = new CountryDto()
                .id(expectedId)
                .name(expectedName);

        Mockito.when(mockCountryFacade.findById(eq(expectedId)))
                .thenReturn(Optional.of(expectedResponse));

        var response = mockMvc.perform(get("/api/countries/4242"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedId))
                .andExpect(jsonPath("$.name").value(expectedName))
                .andReturn().getResponse().getContentAsString();
        log.debug("response: {}", response);

        var countryDtoResponse = objectMapper.readValue(response, CountryDto.class);
        assertThat(countryDtoResponse.getId()).isEqualTo(expectedId);
        assertThat(countryDtoResponse.getName()).isEqualTo(expectedName);
    }
}
