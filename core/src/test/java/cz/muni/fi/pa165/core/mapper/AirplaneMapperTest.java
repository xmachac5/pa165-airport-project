package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.model.AirplaneDto;
import cz.muni.fi.pa165.core.model.NewAirplaneDtoRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AirplaneMapperTest {

    @Autowired
    private AirplaneMapper airplaneMapper;

    @Test
    void toEntityFromNewRequest() {
        var newAirplaneDtoRequest = new NewAirplaneDtoRequest();
        newAirplaneDtoRequest.setName("DL7132");
        newAirplaneDtoRequest.setCapacity(191);

        Airplane airplane = airplaneMapper.toEntityFromNewRequest(newAirplaneDtoRequest);

        assertThat(airplane.getName())
                .isEqualTo(newAirplaneDtoRequest.getName());
        assertThat(airplane.getCapacity())
                .isEqualTo(newAirplaneDtoRequest.getCapacity());
    }

    @Test
    void toDto() {
        Airplane airplane = new Airplane();
        airplane.setId(1L);
        airplane.setName("KL4242");
        airplane.setCapacity(210);
        var airplaneType = new AirplaneType();
        airplaneType.setId(81L);
        airplaneType.setName("Boeing 777-Max");
        airplane.setType(airplaneType);

        AirplaneDto airplaneDto = airplaneMapper.toDto(airplane);

        assertThat(airplaneDto.getId())
                .isEqualTo(airplane.getId());
        assertThat(airplaneDto.getName())
                .isEqualTo(airplane.getName());
        assertThat(airplaneDto.getCapacity())
                .isEqualTo(airplane.getCapacity());
        assertThat(airplaneDto.getTypeId())
                .isEqualTo(airplane.getType().getId());
    }
}