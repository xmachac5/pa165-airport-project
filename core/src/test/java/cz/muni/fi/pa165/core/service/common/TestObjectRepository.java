package cz.muni.fi.pa165.core.service.common;

import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestObjectRepository extends BaseRepository<TestObject, Long> {

}
