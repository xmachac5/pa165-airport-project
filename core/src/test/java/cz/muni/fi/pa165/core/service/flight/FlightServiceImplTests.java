package cz.muni.fi.pa165.core.service.flight;

import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.data.repository.flight.FlightRepository;
import cz.muni.fi.pa165.core.service.flight.FlightServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class FlightServiceImplTests {
    private FlightServiceImpl flightService;
    private FlightRepository flightRepository;


    @BeforeEach
    void setUp() {
        flightRepository = mock(FlightRepository.class);
        flightService = new FlightServiceImpl(flightRepository);
    }

    @Test
    void findByIdTestSuccess() {
        var flightDepartureTime = OffsetDateTime.now();
        var flightArrivalTime = OffsetDateTime.now();
        var flight = new Flight();
        flight.setArrivalTime(flightArrivalTime);
        flight.setDepartureTime(flightDepartureTime);

        when(flightRepository.findById(flight.getId()))
                .thenReturn(Optional.of(flight));

        var foundFlightByIdOpt = flightService.findById(flight.getId());

        assertTrue(foundFlightByIdOpt.isPresent());
        assertEquals(flight.getArrivalTime(), foundFlightByIdOpt.get().getArrivalTime());
        assertEquals(flight.getDepartureTime(), foundFlightByIdOpt.get().getDepartureTime());
    }

    @Test
    void findWithStewardsTestSuccess() {
        var flightDepartureTime = OffsetDateTime.now();
        var flightArrivalTime = OffsetDateTime.now();
        var flight = new Flight();
        flight.setArrivalTime(flightArrivalTime);
        flight.setDepartureTime(flightDepartureTime);

        when(flightRepository.findByIdWithStewards(flight.getId()))
                .thenReturn(Optional.of(flight));

        var foundFlightByIdOpt = flightService.findByIdWithStewards(flight.getId());

        assertTrue(foundFlightByIdOpt.isPresent());
        assertEquals(flight.getFlightStewards(), foundFlightByIdOpt.get().getFlightStewards());
    }

    @Test
    void updateTestSuccess() {
        var flightToUpdate = new Flight();
        flightToUpdate.setId(1L);
        var time = OffsetDateTime.of(2000, 4, 9, 20, 15, 45, 345875000, ZoneOffset.of("+07:00"));
        flightToUpdate.setDepartureTime(time);
        flightToUpdate.setArrivalTime(time);


        var updatedFlightRequest = new Flight();
        var newTime = OffsetDateTime.of(1980, 4, 9, 20, 15, 45, 345875000, ZoneOffset.of("+07:00"));
        updatedFlightRequest.setDepartureTime(newTime);
        updatedFlightRequest.setArrivalTime(newTime);

        when(flightRepository.findById(1L))
                .thenReturn(Optional.of(flightToUpdate));
        when(flightRepository.save(flightToUpdate))
                .thenReturn(updatedFlightRequest);


        assertEquals(time, flightToUpdate.getDepartureTime());
        assertNotEquals(newTime, flightToUpdate.getDepartureTime());
        assertEquals(time, flightToUpdate.getArrivalTime());
        assertNotEquals(newTime, flightToUpdate.getArrivalTime());

        var afterUpdate = flightService.update(1L, updatedFlightRequest);

        assertEquals(afterUpdate.getDepartureTime(), flightToUpdate.getDepartureTime());

    }

}
