package cz.muni.fi.pa165.core.service.common;

import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestObjectServiceImpl extends BaseServiceImpl<TestObject, Long> implements TestObjectService{

    private final TestObjectRepository testObjectRepository;

    @Autowired
    public TestObjectServiceImpl(TestObjectRepository testObjectRepository) {
        super(testObjectRepository);
        this.testObjectRepository = testObjectRepository;
    }
}
