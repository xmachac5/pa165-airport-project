package cz.muni.fi.pa165.core.service.country;

import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.data.repository.country.CountryRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CountryServiceImplTest {

    private CountryServiceImpl countryService;
    private CountryRepository countryRepository;

    @BeforeEach
    void setUp() {
        countryRepository = mock(CountryRepository.class);
        countryService = new CountryServiceImpl(countryRepository);
    }

    @Test
    void findByNameTestSuccess() {
        var countryName = "Italy";
        var country = new Country();
        country.setId(1L);
        country.setName(countryName);

        when(countryRepository.findByName(countryName))
                .thenReturn(Optional.of(country));

        var foundCountryByNameOpt = countryService.findByName(countryName);

        assertTrue(foundCountryByNameOpt.isPresent());
        assertEquals(country.getName(), foundCountryByNameOpt.get().getName());
        assertEquals(country, foundCountryByNameOpt.get());
    }

    @Test
    void updateTestNotFoundByIdThrowsException() {
        var country = new Country();
        country.setId(-1L);
        country.setName("France");

        when(countryRepository.findById(-1L))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> countryService.update(-1L, country));
    }

    @Test
    void updateTestSuccess() {
        var countryToUpdate = new Country();
        countryToUpdate.setId(1L);
        countryToUpdate.setName("France");

        var updatedCountryRequest = new Country();
        updatedCountryRequest.setName("Switzerland");

        when(countryRepository.findById(1L))
                .thenReturn(Optional.of(countryToUpdate));
        when(countryRepository.save(countryToUpdate))
                .thenReturn(updatedCountryRequest);

        var updatedCountry = countryService.update(1L, updatedCountryRequest);

        assertEquals(countryToUpdate.getName(), updatedCountry.getName());
        assertEquals(countryToUpdate, updatedCountry);
    }
}
