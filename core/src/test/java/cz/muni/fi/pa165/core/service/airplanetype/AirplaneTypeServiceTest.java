package cz.muni.fi.pa165.core.service.airplanetype;

import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.data.repository.airplanetype.AirplaneTypeRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AirplaneTypeServiceTest {

    @Autowired
    private AirplaneTypeService airplaneTypeService;

    @MockBean
    private AirplaneTypeRepository airplaneTypeRepository;

    private static final AirplaneType airplaneType = new AirplaneType();

    @BeforeAll
    static void beforeAll() {
        airplaneType.setId(42L);
        airplaneType.setName("A320");
    }

    @Test
    void findByNameWhenPresent() {
        when(airplaneTypeRepository.findByName(anyString()))
                .thenReturn(Optional.of(airplaneType));

        Optional<AirplaneType> actualAirplaneType = airplaneTypeService.findByName("A320");

        verify(airplaneTypeRepository, times(1))
                .findByName("A320");
        verifyNoMoreInteractions(airplaneTypeRepository);
        assertThat(actualAirplaneType.isPresent()).isTrue();
        assertThat(actualAirplaneType.get().getId()).isEqualTo(airplaneType.getId());
        assertThat(actualAirplaneType.get().getName()).isEqualTo(airplaneType.getName());
    }

    @Test
    void findByNameWhenNotPresent() {
        when(airplaneTypeRepository.findByName(anyString()))
                .thenReturn(Optional.empty());

        Optional<AirplaneType> actualAirplaneType = airplaneTypeService.findByName("A320");

        verify(airplaneTypeRepository, times(1))
                .findByName("A320");
        verifyNoMoreInteractions(airplaneTypeRepository);
        assertThat(actualAirplaneType.isEmpty()).isTrue();
    }
}