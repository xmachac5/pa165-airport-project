package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.model.CityDto;
import cz.muni.fi.pa165.core.model.NewCityDtoRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CityMapperTest {

    @Autowired
    private CityMapper cityMapper;

    @Test
    void toEntityFromNewRequest() {
        var newCityDtoRequest = new NewCityDtoRequest();
        newCityDtoRequest.setName("Winnipeg");

        City city = cityMapper.toEntityFromNewRequest(newCityDtoRequest);

        assertThat(city.getName())
                .isEqualTo(newCityDtoRequest.getName());
    }

    @Test
    void toDto() {
        var city = new City();
        city.setId(1L);
        city.setName("Winnipeg");
        var canada = new Country();
        canada.setId(1L);
        canada.setName("Canada");
        city.setCountry(canada);
        city.setAirports(createAirports());

        CityDto cityDto = cityMapper.toDto(city);

        assertThat(cityDto.getId())
                .isEqualTo(city.getId());
        assertThat(cityDto.getName())
                .isEqualTo(city.getName());
        assertThat(cityDto.getCountryId())
                .isEqualTo(city.getCountry().getId());
        assertThat(cityDto.getAirportsIds())
                .hasSameSizeAs(city.getAirports());
        for (int i = 0; i < cityDto.getAirportsIds().size(); i++) {
            assertThat(cityDto.getAirportsIds().get(i))
                    .isEqualTo(city.getAirports().get(i).getId());
        }
    }

    @Test
    void getCountryId() {
        Long countryId = cityMapper.getCountryId(null);

        assertThat(countryId)
                .isZero();
    }

    private List<Airport> createAirports() {
        var yvr = new Airport();
        yvr.setId(42L);
        yvr.setCode("YVR");
        yvr.setName("Vancouver International Airport");

        var yvz = new Airport();
        yvz.setId(13L);
        yvz.setCode("YVZ");
        yvz.setName("Toronto Pearson International Airport");

        var yyc = new Airport();
        yyc.setId(181L);
        yyc.setCode("YYC");
        yyc.setName("Calgary International Airport");

        return List.of(yvr, yvz, yyc);
    }
}