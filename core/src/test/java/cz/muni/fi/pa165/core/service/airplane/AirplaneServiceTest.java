package cz.muni.fi.pa165.core.service.airplane;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.data.repository.airplane.AirplaneRepository;
import cz.muni.fi.pa165.core.data.repository.airplanetype.AirplaneTypeRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SpringBootTest
class AirplaneServiceTest {

    @Autowired
    private AirplaneService airplaneService;

    @MockBean
    private AirplaneRepository airplaneRepository;

    @MockBean
    private AirplaneTypeRepository airplaneTypeRepository;

    private static final Airplane airplane = new Airplane();
    private static final AirplaneType airplaneType = new AirplaneType();

    @BeforeAll
    static void beforeAll() {
        airplane.setId(42L);
        airplane.setName("MH370");
        airplaneType.setId(42L);
        airplaneType.setName("Boeing 777-200ER");
        airplane.setType(airplaneType);
        airplane.setCapacity(239);
    }

    @Test
    void updateWhenPresent() {
        Airplane airplaneToUpdate = new Airplane();
        airplaneToUpdate.setId(1L);
        airplaneToUpdate.setName("MH17");

        when(airplaneRepository.findById(1L))
                .thenReturn(Optional.of(airplaneToUpdate));
        when(airplaneTypeRepository.findById(42L))
                .thenReturn(Optional.of(airplaneType));
        when(airplaneRepository.save(any(Airplane.class)))
                .thenReturn(airplane);

        Airplane actualAirplane = airplaneService.update(1L, airplaneToUpdate);
        assertThat(actualAirplane.getId()).isEqualTo(42L);
        assertThat(actualAirplane.getName()).isEqualTo(airplane.getName());
        assertThat(actualAirplane.getType()).isEqualTo(airplaneType);
        assertThat(actualAirplane.getCapacity()).isEqualTo(airplane.getCapacity());
    }

    @Test
    void updateWhenNotPresent() {
        Airplane airplaneToUpdate = new Airplane();
        airplaneToUpdate.setId(-1L);
        airplaneToUpdate.setName("MH17");
        airplaneToUpdate.setCapacity(298);

        when(airplaneRepository.save(any(Airplane.class)))
                .thenReturn(airplane);

        assertThrows(ResourceNotFoundException.class, () -> airplaneService.update(-1L, airplaneToUpdate));
    }

    @Test
    void save() {
        when(airplaneRepository.save(any(Airplane.class)))
                .thenReturn(airplane);

        Airplane actualAirplane = airplaneService.save(airplane);

        verify(airplaneRepository, times(1))
                .save(airplane);
        verifyNoMoreInteractions(airplaneRepository);
        assertThat(actualAirplane.getId()).isEqualTo(airplane.getId());
        assertThat(actualAirplane.getName()).isEqualTo(airplane.getName());
        assertThat(actualAirplane.getType()).isEqualTo(airplane.getType());
        assertThat(actualAirplane.getCapacity()).isEqualTo(airplane.getCapacity());
    }
}