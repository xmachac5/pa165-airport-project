package cz.muni.fi.pa165.core.service.common;

import org.springframework.stereotype.Service;

@Service
public interface TestObjectService extends BaseService<TestObject, Long> {

}
