package cz.muni.fi.pa165.core.service.common;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * In this test we use {@code BaseServiceImpl<TestObject,Long>}.
 * However, for the sake of not writing everywhere {@code <TestObject,Long>},
 * we created the aliases {@link TestObjectRepository}, {@link TestObjectService}
 * and {@link TestObjectServiceImpl}.
 */
@SpringBootTest
class BaseServiceTest {

    @Autowired
    private TestObjectService testObjectService;

    @MockBean
    private TestObjectRepository testObjectRepository;

    private static final TestObject sampleTestObject = new TestObject("description");
    private static final TestObject sampleTestObject1 = new TestObject("desc1");
    private static final TestObject sampleTestObject2 = new TestObject("desc2");
    private static final List<TestObject> allSampleTestObjects = new ArrayList<>();

    @BeforeAll
    static void beforeAll() {
        sampleTestObject.setId(42L);
        sampleTestObject1.setId(1L);
        sampleTestObject2.setId(2L);
        allSampleTestObjects.addAll(List.of(sampleTestObject1, sampleTestObject2));
    }

    @Test
    void save() {
        when(testObjectRepository.save(any(TestObject.class)))
                .thenReturn(sampleTestObject);
        TestObject actualObject = testObjectService.save(new TestObject(""));
        assertThat(actualObject).isEqualTo(sampleTestObject);
    }

    @Test
    void findById() {
        when(testObjectRepository.findById(42L))
                .thenReturn(Optional.of(sampleTestObject));
        Optional<TestObject> actualObject = testObjectService.findById(42L);
        assertThat(actualObject.isPresent()).isTrue();
        assertThat(actualObject.get()).isEqualTo(sampleTestObject);
    }

    @Test
    void findAll() {
        when(testObjectRepository.findAll())
                .thenReturn(allSampleTestObjects);
        List<TestObject> actualObjects = testObjectService.findAll();
        assertThat(actualObjects.size()).isEqualTo(2);
        assertThat(actualObjects.get(0)).isEqualTo(sampleTestObject1);
        assertThat(actualObjects.get(1)).isEqualTo(sampleTestObject2);
    }

    @Test
    void deleteById() {
        testObjectService.deleteById(2L);

        verify(testObjectRepository, times(1)).deleteById(2L);
        verifyNoMoreInteractions(testObjectRepository);
    }

    @Test
    void deleteAll() {
        testObjectService.deleteAll();

        verify(testObjectRepository, times(1)).deleteAll();
        verifyNoMoreInteractions(testObjectRepository);
    }

    @Test
    void existsById() {
        testObjectService.existsById(1L);

        verify(testObjectRepository, times(1)).existsById(1L);
        verifyNoMoreInteractions(testObjectRepository);
    }
}
