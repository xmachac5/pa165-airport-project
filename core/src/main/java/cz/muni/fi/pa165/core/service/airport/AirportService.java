package cz.muni.fi.pa165.core.service.airport;

import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.service.common.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface AirportService extends BaseService<Airport, Long> {

    Optional<Airport> findByName(String name);

    Optional<Airport> findByCode(String code);

    List<Airport> findByCity(City city);

    Airport update(Long id, Airport updatedAirport);
}
