package cz.muni.fi.pa165.core.data.domain;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Entity(name = "airplanes")
@Data
@NoArgsConstructor
public class Airplane extends DomainEntity {

    @NotNull
    @NotBlank
    @Column(unique = true)
    private String name;

    @NotNull
    @PositiveOrZero
    private Integer capacity;

    @ManyToOne(fetch = FetchType.LAZY)
    private AirplaneType type;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Airplane airplane)) {
            return false;
        }
        return Objects.equals(getName(), airplane.getName()) &&
                Objects.equals(getCapacity(), airplane.getCapacity()) && Objects.equals(getType(), airplane.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCapacity(), getType());
    }
}
