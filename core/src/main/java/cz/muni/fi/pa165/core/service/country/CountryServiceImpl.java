package cz.muni.fi.pa165.core.service.country;

import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.data.repository.country.CountryRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class CountryServiceImpl extends BaseServiceImpl<Country, Long> implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        super(countryRepository);
        this.countryRepository = countryRepository;
    }

    @Transactional
    @Override
    public Optional<Country> findByName(String name) {
        return countryRepository.findByName(name);
    }

    @Transactional
    @Override
    public Country update(Long id, Country updatedCountry) {
        Country countryToUpdate = countryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Country with id " + id + " not found."));

        countryToUpdate.setName(updatedCountry.getName());

        return countryRepository.save(countryToUpdate);
    }
}
