package cz.muni.fi.pa165.core.facade.steward;

import cz.muni.fi.pa165.core.data.domain.FlightSteward;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.mapper.StewardMapper;
import cz.muni.fi.pa165.core.model.NewStewardDtoRequest;
import cz.muni.fi.pa165.core.model.StewardDto;
import cz.muni.fi.pa165.core.service.flight.FlightService;
import cz.muni.fi.pa165.core.service.steward.StewardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StewardFacadeImpl implements StewardFacade<Long> {

    private final StewardService stewardService;
    private final FlightService flightService;
    private final StewardMapper stewardMapper;


    @Autowired
    public StewardFacadeImpl(StewardService stewardService, FlightService flightService, StewardMapper stewardMapper) {
        this.stewardService = stewardService;
        this.flightService = flightService;
        this.stewardMapper = stewardMapper;
    }

    @Override
    public StewardDto save(NewStewardDtoRequest newStewardDtoRequest) {
        var entityToSave = stewardMapper.toEntityFromNewRequest(newStewardDtoRequest);
        var savedEntity = stewardService.save(entityToSave);
        return stewardMapper.toDto(savedEntity);
    }

    @Override
    public Optional<StewardDto> findById(Long id) {
        var foundStewardEntity = stewardService.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        var stewardDto = stewardMapper.toDto(foundStewardEntity);

        return Optional.of(stewardDto);
    }

    @Override
    public List<StewardDto> findAll() {
        var foundStewards = stewardService.findAll();

        return foundStewards
                .stream()
                .map(stewardMapper::toDto)
                .toList();
    }

    @Override
    public void deleteById(Long id) {
        if (!stewardService.existsById(id)) {
            throw new ResourceNotFoundException();
        }
        stewardService.deleteById(id);
    }

    @Override
    public StewardDto update(Long id, NewStewardDtoRequest newStewardDtoRequest) {
        if (!stewardService.existsById(id)) {
            throw new ResourceNotFoundException();
        }
        var newStewardEntity = stewardMapper.toEntityFromNewRequest(newStewardDtoRequest);
        var updatedStewardEntity = stewardService.update(id, newStewardEntity);

        return stewardMapper.toDto(updatedStewardEntity);
    }

    @Override
    public StewardDto assignStewardFlight(Long stewardId, Long flightId) {
        var stewardEntity = stewardService.findByIdWithFlights(stewardId)
                .orElse(stewardService.findById(stewardId)
                        .orElseThrow(ResourceNotFoundException::new));

        var flightEntity = flightService.findById(flightId)
                .orElseThrow(ResourceNotFoundException::new);

        var flightSteward = new FlightSteward();
        flightSteward.setSteward(stewardEntity);
        flightSteward.setFlight(flightEntity);
        stewardService.saveFlightStewards(flightSteward);

        return stewardMapper.toDto(stewardEntity);
    }

    @Override
    public void deleteStewardFlightsAssignment(Long stewardId, Long flightId) {
        var stewardEntity = stewardService.findById(stewardId)
                        .orElseThrow(ResourceNotFoundException::new);

        var flightEntity = flightService.findById(flightId)
                .orElseThrow(ResourceNotFoundException::new);

        stewardService.deleteFlightStewards(stewardEntity, flightEntity);
    }
}
