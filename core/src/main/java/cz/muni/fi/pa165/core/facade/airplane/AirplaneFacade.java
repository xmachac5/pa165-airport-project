package cz.muni.fi.pa165.core.facade.airplane;

import cz.muni.fi.pa165.core.model.AirplaneDto;
import cz.muni.fi.pa165.core.model.NewAirplaneDtoRequest;

import java.util.List;
import java.util.Optional;

/**
 * @param <K> Key
 * @author martinslovik
 */
public interface AirplaneFacade<K> {

    AirplaneDto save(NewAirplaneDtoRequest newAirplaneDtoRequest);

    Optional<AirplaneDto> findById(K id);

    List<AirplaneDto> findAll();

    void deleteById(K id);

    void deleteAll();

    AirplaneDto update(Long id, NewAirplaneDtoRequest newAirplaneDtoRequest);
}
