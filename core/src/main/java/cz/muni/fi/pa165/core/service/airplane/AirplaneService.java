package cz.muni.fi.pa165.core.service.airplane;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.service.common.BaseService;

public interface AirplaneService extends BaseService<Airplane, Long> {

    Airplane update(Long id, Airplane newAirplane);
}
