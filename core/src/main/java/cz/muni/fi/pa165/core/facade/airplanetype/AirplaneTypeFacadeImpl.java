package cz.muni.fi.pa165.core.facade.airplanetype;

import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.mapper.AirplaneTypeMapper;
import cz.muni.fi.pa165.core.model.AirplaneTypeDto;
import cz.muni.fi.pa165.core.model.NewAirplaneTypeDtoRequest;
import cz.muni.fi.pa165.core.service.airplanetype.AirplaneTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AirplaneTypeFacadeImpl implements AirplaneTypeFacade<Long> {

    private final AirplaneTypeService airplaneTypeService;
    private final AirplaneTypeMapper airplaneTypeMapper;

    @Autowired
    public AirplaneTypeFacadeImpl(AirplaneTypeService airplaneTypeService, AirplaneTypeMapper airplaneTypeMapper) {
        this.airplaneTypeService = airplaneTypeService;
        this.airplaneTypeMapper = airplaneTypeMapper;
    }

    @Override
    public AirplaneTypeDto save(NewAirplaneTypeDtoRequest newAirplaneTypeDtoRequest) {
        var entityToSave = airplaneTypeMapper.toEntityFromNewRequest(newAirplaneTypeDtoRequest);
        var savedEntity = airplaneTypeService.save(entityToSave);
        return airplaneTypeMapper.toDto(savedEntity);
    }

    @Override
    public Optional<AirplaneTypeDto> findById(Long id) {
        var foundEntity = airplaneTypeService.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        return Optional.ofNullable(airplaneTypeMapper.toDto(foundEntity));
    }

    @Override
    public List<AirplaneTypeDto> findAll() {
        var entities = airplaneTypeService.findAll();
        return entities.stream()
                .map(airplaneTypeMapper::toDto)
                .toList();
    }

    @Override
    public void deleteById(Long id) {
        airplaneTypeService.deleteById(id);
    }

    @Override
    public void deleteAll() {
        airplaneTypeService.deleteAll();
    }

    @Override
    public AirplaneTypeDto update(Long id, NewAirplaneTypeDtoRequest newAirplaneTypeDtoRequest) {
        var newAirplaneTypeEntity = airplaneTypeMapper.toEntityFromNewRequest(newAirplaneTypeDtoRequest);

        return airplaneTypeMapper.toDto(airplaneTypeService.update(id, newAirplaneTypeEntity));
    }

}
