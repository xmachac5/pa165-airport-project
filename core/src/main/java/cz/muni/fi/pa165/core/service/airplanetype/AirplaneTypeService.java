package cz.muni.fi.pa165.core.service.airplanetype;

import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.service.common.BaseService;

import java.util.Optional;

public interface AirplaneTypeService extends BaseService<AirplaneType, Long> {

    Optional<AirplaneType> findByName(String name);

    AirplaneType update(Long id, AirplaneType newAirplaneType);
}