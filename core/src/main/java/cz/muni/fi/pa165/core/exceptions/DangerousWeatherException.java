package cz.muni.fi.pa165.core.exceptions;

/**
 * Exception thrown in case of {@code FlightCreationAdvice} does not recommend
 * us to create the flight.
 */
public class DangerousWeatherException extends RuntimeException {

    public DangerousWeatherException(String message) {
        super(message);
    }
}
