package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.model.AirplaneDto;
import cz.muni.fi.pa165.core.model.NewAirplaneDtoRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public abstract class AirplaneMapper {

    @Autowired
    private AirplaneTypeMapper airplaneTypeMapper;

    public abstract Airplane toEntityFromNewRequest(NewAirplaneDtoRequest newAirplaneDtoRequest);

    @Mapping(target = "typeId", source = "type")
    public abstract AirplaneDto toDto(Airplane airplane);

    protected Long getAirplanesTypeId(AirplaneType airplaneType) {
        return airplaneTypeMapper.getAirplanesTypeId(airplaneType);
    }
}
