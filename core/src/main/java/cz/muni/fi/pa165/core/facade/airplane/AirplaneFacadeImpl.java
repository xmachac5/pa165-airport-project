package cz.muni.fi.pa165.core.facade.airplane;

import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.mapper.AirplaneMapper;
import cz.muni.fi.pa165.core.model.AirplaneDto;
import cz.muni.fi.pa165.core.model.NewAirplaneDtoRequest;
import cz.muni.fi.pa165.core.service.airplane.AirplaneService;
import cz.muni.fi.pa165.core.service.airplanetype.AirplaneTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AirplaneFacadeImpl implements AirplaneFacade<Long> {

    private final AirplaneService airplaneService;
    private final AirplaneTypeService airplaneTypeService;
    private final AirplaneMapper airplaneMapper;

    @Autowired
    public AirplaneFacadeImpl(AirplaneService airplaneService, AirplaneTypeService airplaneTypeService, AirplaneMapper airplaneMapper) {
        this.airplaneService = airplaneService;
        this.airplaneTypeService = airplaneTypeService;
        this.airplaneMapper = airplaneMapper;
    }

    @Override
    public AirplaneDto save(NewAirplaneDtoRequest newAirplaneDtoRequest) {
        var airplaneType = airplaneTypeService.findById(newAirplaneDtoRequest.getTypeId())
                .orElseThrow(ResourceNotFoundException::new);
        var entityToSave = airplaneMapper.toEntityFromNewRequest(newAirplaneDtoRequest);
        entityToSave.setType(airplaneType);
        var savedEntity = airplaneService.save(entityToSave);
        return airplaneMapper.toDto(savedEntity);
    }

    @Override
    public Optional<AirplaneDto> findById(Long id) {
        var foundEntity = airplaneService.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        return Optional.ofNullable(airplaneMapper.toDto(foundEntity));
    }

    @Override
    public List<AirplaneDto> findAll() {
        var entities = airplaneService.findAll();
        return entities.stream()
                .map(airplaneMapper::toDto)
                .toList();
    }

    @Override
    public void deleteById(Long id) {
        airplaneService.deleteById(id);
    }

    @Override
    public void deleteAll() {
        airplaneService.deleteAll();
    }

    @Override
    public AirplaneDto update(Long id, NewAirplaneDtoRequest newAirplaneDtoRequest) {
        var newAirplaneEntity = airplaneMapper.toEntityFromNewRequest(newAirplaneDtoRequest);

        var savedEntity = airplaneService.update(id, newAirplaneEntity);

        return airplaneMapper.toDto(savedEntity);
    }
}
