package cz.muni.fi.pa165.core.facade.city;

import cz.muni.fi.pa165.core.model.CityDto;
import cz.muni.fi.pa165.core.model.NewCityDtoRequest;

import java.util.List;
import java.util.Optional;

/**
 * @param <K> Key
 */
public interface CityFacade<K> {

    CityDto save(NewCityDtoRequest newCityDtoRequest);

    Optional<CityDto> findById(K id);

    List<CityDto> findAll();

    void deleteById(K id);

    void deleteAll();

    CityDto update(K id, NewCityDtoRequest newCityDtoRequest);

    Optional<CityDto> findByName(String name);

    CityDto addCountryAssignment(K cityId, K countryId);

    void deleteCountryAssignment(K cityId, K countryId);
}
