package cz.muni.fi.pa165.core.facade.city;

import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.mapper.CityMapper;
import cz.muni.fi.pa165.core.model.CityDto;
import cz.muni.fi.pa165.core.model.NewCityDtoRequest;
import cz.muni.fi.pa165.core.service.city.CityService;
import cz.muni.fi.pa165.core.service.country.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityFacadeImpl implements CityFacade<Long> {

    private final CityService cityService;
    private final CountryService countryService;
    private final CityMapper cityMapper;

    @Autowired
    public CityFacadeImpl(CityService cityService, CountryService countryService, CityMapper cityMapper) {
        this.cityService = cityService;
        this.countryService = countryService;
        this.cityMapper = cityMapper;
    }

    @Override
    public CityDto save(NewCityDtoRequest newCityDtoRequest) {
        City entityToSave = cityMapper.toEntityFromNewRequest(newCityDtoRequest);
        City savedEntity = cityService.save(entityToSave);
        return cityMapper.toDto(savedEntity);
    }

    @Override
    public Optional<CityDto> findById(Long id) {
        City foundEntity = cityService.findById(id)
                .orElseThrow(ResourceNotFoundException::new);
        return Optional.of(cityMapper.toDto(foundEntity));
    }

    @Override
    public List<CityDto> findAll() {
        List<City> foundEntities = cityService.findAll();
        return foundEntities.stream()
                .map(cityMapper::toDto)
                .toList();
    }

    @Override
    public void deleteById(Long id) {
        if (!cityService.existsById(id)) {
            throw new ResourceNotFoundException();
        }
        cityService.deleteById(id);
    }

    @Override
    public void deleteAll() {
        cityService.deleteAll();
    }

    @Override
    public CityDto update(Long id, NewCityDtoRequest newCityDtoRequest) {
        if (!cityService.existsById(id)) {
            throw new ResourceNotFoundException();
        }

        City newEntity = cityMapper.toEntityFromNewRequest(newCityDtoRequest);
        City updatedEntity = cityService.update(id, newEntity);
        return cityMapper.toDto(updatedEntity);
    }

    @Override
    public Optional<CityDto> findByName(String name) {
        Optional<City> foundEntity = cityService.findByName(name);
        return foundEntity.map(cityMapper::toDto);
    }

    @Override
    public CityDto addCountryAssignment(Long cityId, Long countryId) {
        City city = cityService.findById(cityId)
                .orElseThrow(ResourceNotFoundException::new);
        Country country = countryService.findById(countryId)
                .orElseThrow(ResourceNotFoundException::new);

        country.addCity(city);
        city.setCountry(country);

        countryService.update(countryId, country);
        City updatedCity = cityService.update(cityId, city);

        return cityMapper.toDto(updatedCity);
    }

    @Override
    public void deleteCountryAssignment(Long cityId, Long countryId) {
        City city = cityService.findById(cityId)
                .orElseThrow(ResourceNotFoundException::new);
        Country country = countryService.findById(countryId)
                .orElseThrow(ResourceNotFoundException::new);

        country.removeCity(city);
        city.setCountry(null);

        countryService.update(countryId, country);
        cityService.update(cityId, city);
    }
}
