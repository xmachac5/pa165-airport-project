package cz.muni.fi.pa165.core.data.domain;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "airplane_types")
@Data
@NoArgsConstructor
public class AirplaneType extends DomainEntity {

    @NotNull
    @NotBlank
    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL)
    private List<Airplane> airplanes = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AirplaneType airplaneType)) {
            return false;
        }
        return Objects.equals(getName(), airplaneType.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
