package cz.muni.fi.pa165.core.config;

import cz.muni.fi.pa165.user.client.Authorities;
import cz.muni.fi.pa165.user.client.UserServiceInterceptionConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@Import(UserServiceInterceptionConfigurer.class)
public class SecurityConfig {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(x -> x
                        // swagger:
                        .requestMatchers("/swagger-ui/**").permitAll()
                        .requestMatchers("/v3/api-docs/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/").permitAll()
                        .requestMatchers(HttpMethod.GET, "/swagger-ui.html").permitAll()

                        // actuator
                        .requestMatchers("/actuator/**").permitAll()

                        // Manager can read everything
                        .requestMatchers(HttpMethod.GET, "/api/**").hasAuthority(Authorities.MANAGER)

                        // Manager can create/update/delete flights
                        .requestMatchers(HttpMethod.POST, "/api/flights").hasAuthority(Authorities.MANAGER)
                        .requestMatchers(HttpMethod.PUT, "/api/flights/*").hasAuthority(Authorities.MANAGER)
                        .requestMatchers(HttpMethod.DELETE, "/api/flights/*").hasAuthority(Authorities.MANAGER)

                        // Manager can create/update/delete stewards
                        .requestMatchers(HttpMethod.POST, "/api/stewards").hasAuthority(Authorities.MANAGER)
                        .requestMatchers(HttpMethod.PUT, "/api/stewards/*").hasAuthority(Authorities.MANAGER)
                        .requestMatchers(HttpMethod.DELETE, "/api/stewards/*").hasAuthority(Authorities.MANAGER)

                        // Manager can create/update/delete airplanes
                        .requestMatchers(HttpMethod.POST, "/api/airplanes").hasAuthority(Authorities.MANAGER)
                        .requestMatchers(HttpMethod.PUT, "/api/airplanes/*").hasAuthority(Authorities.MANAGER)
                        .requestMatchers(HttpMethod.DELETE, "/api/airplanes/*").hasAuthority(Authorities.MANAGER)

                        // Manager can assign flights
                        .requestMatchers("/api/airports/*/departingFlights/*").hasAuthority(Authorities.MANAGER)
                        .requestMatchers("/api/airports/*/arrivingFlights/*").hasAuthority(Authorities.MANAGER)

                        // Manager can assign stewards
                        .requestMatchers("/api/stewards/*/flights/*").hasAuthority(Authorities.MANAGER)

                        // Administrator can create/update/delete airplane types
                        .requestMatchers(HttpMethod.GET, "/api/airplaneTypes").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.GET, "/api/airplaneTypes/*").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.POST, "/api/airplaneTypes").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.PUT, "/api/airplaneTypes/*").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.DELETE, "/api/airplaneTypes/*").hasAuthority(Authorities.ADMINISTRATOR)

                        // Administrator can create/update/delete countries
                        .requestMatchers(HttpMethod.GET, "/api/countries").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.GET, "/api/countries/*").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.POST, "/api/countries").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.PUT, "/api/countries/*").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.DELETE, "/api/countries/*").hasAuthority(Authorities.ADMINISTRATOR)

                        // Administrator can create/update/delete/assign (do everything) with cities
                        .requestMatchers("/api/cities").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers("/api/cities/**").hasAuthority(Authorities.ADMINISTRATOR)

                        // Administrator read/create/update/delete countries
                        .requestMatchers(HttpMethod.GET, "/api/countries").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.GET, "/api/countries/*").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.POST, "/api/countries").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.PUT, "/api/countries/*").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.DELETE, "/api/countries/*").hasAuthority(Authorities.ADMINISTRATOR)

                        // Administrator read/create/update/delete airports
                        .requestMatchers(HttpMethod.GET, "/api/airports").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.GET, "/api/airports/*").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.POST, "/api/airports").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.PUT, "/api/airports/*").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.DELETE, "/api/airports/*").hasAuthority(Authorities.ADMINISTRATOR)

                        // Administrator can seed and clear predefined data db
                        .requestMatchers(HttpMethod.POST, "/api/db/seed").hasAuthority(Authorities.ADMINISTRATOR)
                        .requestMatchers(HttpMethod.DELETE, "/api/db/clear").hasAuthority(Authorities.ADMINISTRATOR)

                        // deny everything else
                        .anyRequest().denyAll()
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken);
        return http.build();
    }
}
