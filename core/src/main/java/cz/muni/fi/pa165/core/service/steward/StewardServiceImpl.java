package cz.muni.fi.pa165.core.service.steward;

import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.data.domain.FlightSteward;
import cz.muni.fi.pa165.core.data.domain.Steward;
import cz.muni.fi.pa165.core.data.repository.flightsteward.FlightStewardRepository;
import cz.muni.fi.pa165.core.data.repository.steward.StewardRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StewardServiceImpl extends BaseServiceImpl<Steward, Long> implements StewardService {

    private final StewardRepository stewardRepository;
    private final FlightStewardRepository flightStewardRepository;

    @Autowired
    public StewardServiceImpl(StewardRepository stewardRepository, FlightStewardRepository flightStewardRepository) {
        super(stewardRepository);
        this.stewardRepository = stewardRepository;
        this.flightStewardRepository = flightStewardRepository;
    }

    @Override
    public FlightSteward saveFlightStewards(FlightSteward flightSteward) {
        return flightStewardRepository.save(flightSteward);
    }

    @Override
    public Optional<Steward> findByIdWithFlights(Long id) {
        return stewardRepository.findByIdWithFlights(id);
    }

    @Override
    public void deleteFlightStewards(Steward steward, Flight flight) {
        var foundFlightStewards = flightStewardRepository.findByStewardAndFlight(steward, flight)
                .orElseThrow(ResourceNotFoundException::new);
        flightStewardRepository.delete(foundFlightStewards);
    }

    @Override
    public Steward update(Long id, Steward newSteward) {
        var entityToUpdate = stewardRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        entityToUpdate.setFirstName(newSteward.getFirstName());
        entityToUpdate.setLastName(newSteward.getLastName());

        return stewardRepository.save(entityToUpdate);
    }
}
