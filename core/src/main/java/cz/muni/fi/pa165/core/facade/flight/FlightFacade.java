package cz.muni.fi.pa165.core.facade.flight;

import cz.muni.fi.pa165.core.model.FlightDto;
import cz.muni.fi.pa165.core.model.NewFlightDtoRequest;

import java.util.List;
import java.util.Optional;

/**
 * @param <K> Key
 * @author martinslovik
 */
public interface FlightFacade<K> {

    FlightDto save(NewFlightDtoRequest newFlightDtoRequest);

    Optional<FlightDto> findById(K id);

    List<FlightDto> findAll();

    void deleteById(K id);

    FlightDto update(Long id, NewFlightDtoRequest newFlightDtoRequest);
}
