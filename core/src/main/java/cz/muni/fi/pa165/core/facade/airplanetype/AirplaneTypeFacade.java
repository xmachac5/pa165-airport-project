package cz.muni.fi.pa165.core.facade.airplanetype;

import cz.muni.fi.pa165.core.model.AirplaneTypeDto;
import cz.muni.fi.pa165.core.model.NewAirplaneTypeDtoRequest;

import java.util.List;
import java.util.Optional;

/**
 * @param <K> Key
 * @author martinslovik
 */
public interface AirplaneTypeFacade<K> {

    AirplaneTypeDto save(NewAirplaneTypeDtoRequest newAirplaneTypeDtoRequest);

    Optional<AirplaneTypeDto> findById(K id);

    List<AirplaneTypeDto> findAll();

    void deleteById(K id);

    void deleteAll();

    AirplaneTypeDto update(K id, NewAirplaneTypeDtoRequest newAirplaneTypeDtoRequest);
}