package cz.muni.fi.pa165.core.rest;

import cz.muni.fi.pa165.core.api.DatabaseApi;
import cz.muni.fi.pa165.core.api.DatabaseApiDelegate;
import cz.muni.fi.pa165.core.data.seed.DatabaseInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DatabaseController implements DatabaseApiDelegate {

    private final DatabaseInitializer databaseInitializer;

    @Autowired
    public DatabaseController(DatabaseInitializer databaseInitializer) {
        this.databaseInitializer = databaseInitializer;
    }

    /**
     * POST /api/db/seed : Seeds the predefined data database
     *
     * @return Created (status code 201)
     * @see DatabaseApi#seed
     */
    @Override
    public ResponseEntity<Void> seed() {
        databaseInitializer.seed();
        return ResponseEntity.ok().build();
    }

    /**
     * DELETE /api/db/clear : Clears the predefined data database
     *
     * @return Deleted (status code 204)
     * @see DatabaseApi#clear
     */
    @Override
    public ResponseEntity<Void> clear() {
        databaseInitializer.clear();
        return ResponseEntity.noContent().build();
    }
}
