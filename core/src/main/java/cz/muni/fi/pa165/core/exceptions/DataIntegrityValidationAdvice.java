package cz.muni.fi.pa165.core.exceptions;

import cz.muni.fi.pa165.core.model.ErrorMessage;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.OffsetDateTime;

@ControllerAdvice
public class DataIntegrityValidationAdvice {

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    ResponseEntity<ErrorMessage> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        var errorMessage = new ErrorMessage();
        errorMessage.setTimestamp(OffsetDateTime.now());
        errorMessage.setStatus(HttpStatus.CONFLICT.value());
        errorMessage.setError(HttpStatus.CONFLICT.getReasonPhrase());
        /*
            Custom exception message for security reasons to prevent showing failed insert SQL statement
            to the Client and therefore revealing our DB tables.
        */
        errorMessage.setMessage("fields must be unique.");
        errorMessage.setPath(ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString());
        return new ResponseEntity<>(errorMessage, HttpStatus.CONFLICT);
    }
}
