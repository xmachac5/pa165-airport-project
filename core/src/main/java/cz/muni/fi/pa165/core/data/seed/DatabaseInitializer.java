package cz.muni.fi.pa165.core.data.seed;

import cz.muni.fi.pa165.core.data.domain.*;
import cz.muni.fi.pa165.core.data.repository.airplane.AirplaneRepository;
import cz.muni.fi.pa165.core.data.repository.airplanetype.AirplaneTypeRepository;
import cz.muni.fi.pa165.core.data.repository.airport.AirportRepository;
import cz.muni.fi.pa165.core.data.repository.city.CityRepository;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import cz.muni.fi.pa165.core.data.repository.country.CountryRepository;
import cz.muni.fi.pa165.core.data.repository.flight.FlightRepository;
import cz.muni.fi.pa165.core.data.repository.flightsteward.FlightStewardRepository;
import cz.muni.fi.pa165.core.data.repository.steward.StewardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseInitializer {

    private final List<BaseRepository<?, ?>> repositories = new ArrayList<>();

    private final AirplaneRepository airplaneRepository;
    private final AirplaneTypeRepository airplaneTypeRepository;
    private final AirportRepository airportRepository;
    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;
    private final FlightRepository flightRepository;
    private final FlightStewardRepository flightStewardRepository;
    private final StewardRepository stewardRepository;

    @Autowired
    public DatabaseInitializer(AirplaneRepository airplaneRepository,
                               AirplaneTypeRepository airplaneTypeRepository,
                               AirportRepository airportRepository,
                               CityRepository cityRepository,
                               CountryRepository countryRepository,
                               FlightRepository flightRepository,
                               FlightStewardRepository flightStewardRepository,
                               StewardRepository stewardRepository) {
        this.airplaneRepository = airplaneRepository;
        this.airplaneTypeRepository = airplaneTypeRepository;
        this.airportRepository = airportRepository;
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
        this.flightRepository = flightRepository;
        this.flightStewardRepository = flightStewardRepository;
        this.stewardRepository = stewardRepository;

        repositories.add(flightRepository);
        repositories.add(flightStewardRepository);
        repositories.add(stewardRepository);
        repositories.add(airportRepository);
        repositories.add(cityRepository);
        repositories.add(countryRepository);
        repositories.add(airplaneTypeRepository);
        repositories.add(airplaneRepository);
    }

    public void clear() {
        repositories.forEach(BaseRepository::deleteAll);
    }

    public void seed() {
        clear();

        var boeing777 = createAirplaneType("Boeing 777");
        airplaneTypeRepository.save(boeing777);

        var freddyTheAirplane = createAirplane("Freddy The Airplane", 100, boeing777);
        airplaneRepository.save(freddyTheAirplane);

        var flight1 = createFlight(OffsetDateTime.parse("2023-12-22T12:04:04.493908908+01:00"),
                OffsetDateTime.parse("2023-12-22T12:05:05.493908908+01:00"), freddyTheAirplane);
        flightRepository.save(flight1);

        var steward1 = createSteward("John", "Doe");
        stewardRepository.save(steward1);

        var steward2 = createSteward("Jane", "Doe");
        stewardRepository.save(steward2);

        var flight1Steward1 = assignFlightToSteward(flight1, steward1);
        flightStewardRepository.save(flight1Steward1);

        var flight1Steward2 = assignFlightToSteward(flight1, steward2);
        flightStewardRepository.save(flight1Steward2);

        var germany = createCountry("Germany");
        countryRepository.save(germany);

        var france = createCountry("France");
        countryRepository.save(france);

        var berlin = createCity("Berlin");
        berlin.setCountry(germany);
        cityRepository.save(berlin);

        var paris = createCity("Paris");
        paris.setCountry(france);
        cityRepository.save(paris);

        var ber = createAirport("Berlin Brandenburg Airport", "BER", 52.3666666667, 13.5033333333);
        ber.setCity(berlin);
        airportRepository.save(ber);
        flight1.setDepartingAirport(ber);
        flightRepository.save(flight1);

        var cdg = createAirport("Paris Charles de Gaulle airport", "CDG", 49.0083899664, 2.53844117956);
        cdg.setCity(paris);
        airportRepository.save(cdg);
        flight1.setArrivingAirport(cdg);
        flightRepository.save(flight1);
    }

    private Country createCountry(String name) {
        var country = new Country();
        country.setName(name);
        return country;
    }

    private City createCity(String name) {
        var city = new City();
        city.setName(name);
        return city;
    }

    private AirplaneType createAirplaneType(String name) {
        var airplaneType = new AirplaneType();
        airplaneType.setName(name);
        return airplaneType;
    }

    private Airplane createAirplane(String name, Integer capacity, AirplaneType airplaneType) {
        var airplane = new Airplane();
        airplane.setName(name);
        airplane.setCapacity(capacity);
        airplane.setType(airplaneType);
        return airplane;
    }

    private Flight createFlight(OffsetDateTime departureTime, OffsetDateTime arrivalTime, Airplane airplane) {
        var flight = new Flight();
        flight.setDepartureTime(departureTime);
        flight.setArrivalTime(arrivalTime);
        flight.setAirplane(airplane);
        return flight;
    }

    private Steward createSteward(String firstName, String lastName) {
        var steward = new Steward();
        steward.setFirstName(firstName);
        steward.setLastName(lastName);
        return steward;
    }

    private FlightSteward assignFlightToSteward(Flight flight, Steward steward) {
        var flightSteward = new FlightSteward();
        flightSteward.setFlight(flight);
        flightSteward.setSteward(steward);
        return flightSteward;
    }

    private Airport createAirport(String name, String code, double latitude, double longitude) {
        var airport = new Airport();
        airport.setName(name);
        airport.setCode(code);
        airport.setLatitude(latitude);
        airport.setLongitude(longitude);
        return airport;
    }
}
