package cz.muni.fi.pa165.core.data.repository.flight;

import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FlightRepository extends BaseRepository<Flight, Long> {

    /**
     * Returns Flight entity with eagerly fetched Stewards
     * @param id flightId
     * @return Optional<Flight>
     */
    @Query("SELECT f FROM Flight f JOIN FETCH f.flightStewards fs WHERE f.id = :id")
    Optional<Flight> findByIdWithStewards(@Param("id") Long id);
}
