package cz.muni.fi.pa165.core.exceptions;

import cz.muni.fi.pa165.core.model.ErrorMessage;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.OffsetDateTime;

@ControllerAdvice
public class ConstraintValidationAdvice {

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    ResponseEntity<ErrorMessage> handleConstraintValidationException(ConstraintViolationException ex) {
        var errorMessage = new ErrorMessage();
        errorMessage.setTimestamp(OffsetDateTime.now());
        errorMessage.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
        errorMessage.setError(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
        errorMessage.setMessage(ex.getMessage());
        errorMessage.setPath(ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString());
        return new ResponseEntity<>(errorMessage, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
