package cz.muni.fi.pa165.core.data.repository.city;

import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CityRepository extends BaseRepository<City, Long> {

    @Query("SELECT c FROM City c WHERE c.name = :name")
    Optional<City> findByName(@Param("name") String name);
}
