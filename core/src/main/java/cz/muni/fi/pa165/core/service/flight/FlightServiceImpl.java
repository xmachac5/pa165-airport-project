package cz.muni.fi.pa165.core.service.flight;

import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.data.repository.flight.FlightRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FlightServiceImpl extends BaseServiceImpl<Flight, Long> implements FlightService {

    private final FlightRepository flightRepository;

    @Autowired
    public FlightServiceImpl(FlightRepository flightRepository) {
        super(flightRepository);
        this.flightRepository = flightRepository;
    }

    @Override
    public Optional<Flight> findByIdWithStewards(Long id) {
        return flightRepository.findByIdWithStewards(id);
    }

    @Override
    public Flight update(Long id, Flight newFlight) {
        var entityToUpdate = flightRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        entityToUpdate.setDepartureTime(newFlight.getDepartureTime());
        entityToUpdate.setArrivalTime(newFlight.getArrivalTime());

        return flightRepository.save(entityToUpdate);
    }
}
