package cz.muni.fi.pa165.core.service.common;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public abstract class BaseServiceImpl<E extends DomainEntity, K> implements BaseService<E, K> {

    private final BaseRepository<E, K> repository;

    @Autowired
    private Validator validator;

    @Autowired
    protected BaseServiceImpl(BaseRepository<E, K> repository) {
        this.repository = repository;
    }

    @Override
    public E save(E entity) {
        var violations = validator.validate(entity);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        return repository.save(entity);
    }

    @Override
    public Optional<E> findById(K id) {
        return repository.findById(id);
    }

    @Override
    public List<E> findAll() {
        return (List<E>) repository.findAll();
    }

    @Override
    public void deleteById(K id) {
        repository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(K id) {
        return repository.existsById(id);
    }
}
