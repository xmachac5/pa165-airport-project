package cz.muni.fi.pa165.core.data.domain;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "airports")
@Data
@NoArgsConstructor
public class Airport extends DomainEntity {

    @NotNull
    @NotBlank
    @Column(unique = true)
    private String name;

    @NotNull
    @Column(unique = true)
    @Size(min = 3, max = 3)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;

    private double latitude;
    private double longitude;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "arrivingAirport")
    private List<Flight> arrivingFlights = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "departingAirport")
    private List<Flight> departingFlights = new ArrayList<>();

    public void addArrivingFlight(Flight arrivingFlight) {
        arrivingFlights.add(arrivingFlight);
    }

    public void removeArrivingFlight(Flight arrivingFlight) {
        arrivingFlights.remove(arrivingFlight);
    }

    public void addDepartingFlight(Flight departingFlight) {
        departingFlights.add(departingFlight);
    }

    public void removeDepartingFlight(Flight departingFlight) {
        departingFlights.remove(departingFlight);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Airport airport)) {
            return false;
        }
        return getName().equals(airport.getName())
                && getCode().equals(airport.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCode());
    }

    @Override
    public String toString() {
        return "Airport{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", city=" + city.getName() +
                '}';
    }
}
