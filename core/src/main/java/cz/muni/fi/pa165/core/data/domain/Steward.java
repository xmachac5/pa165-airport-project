package cz.muni.fi.pa165.core.data.domain;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "stewards")
@Data
@NoArgsConstructor
public class Steward extends DomainEntity {

    @NotNull
    @NotBlank
    private String firstName;

    @NotNull
    @NotBlank
    private String lastName;

    @OneToMany(mappedBy = "steward")
    private List<FlightSteward> flightStewards;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Steward steward)) {
            return false;
        }
        return Objects.equals(getFirstName(), steward.getFirstName()) &&
                Objects.equals(getLastName(), steward.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName());
    }
}
