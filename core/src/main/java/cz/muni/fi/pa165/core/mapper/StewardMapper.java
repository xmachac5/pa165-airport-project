package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.FlightSteward;
import cz.muni.fi.pa165.core.data.domain.Steward;
import cz.muni.fi.pa165.core.model.NewStewardDtoRequest;
import cz.muni.fi.pa165.core.model.StewardDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface StewardMapper {

    Steward toEntityFromNewRequest(NewStewardDtoRequest newStewardDtoRequest);

    default List<Long> toFlightIdList(List<FlightSteward> flightStewards) {
        if (flightStewards != null) {
            return flightStewards.stream()
                    .map(fs -> fs.getFlight().getId())
                    .toList();
        }
        return new ArrayList<>();
    }

    @Mapping(target = "assignedFlightIds", expression = "java(toFlightIdList(steward.getFlightStewards()))")
    StewardDto toDto(Steward steward);
}
