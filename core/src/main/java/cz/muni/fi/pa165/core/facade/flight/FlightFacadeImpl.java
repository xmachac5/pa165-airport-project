package cz.muni.fi.pa165.core.facade.flight;

import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.mapper.FlightMapper;
import cz.muni.fi.pa165.core.model.FlightDto;
import cz.muni.fi.pa165.core.model.NewFlightDtoRequest;
import cz.muni.fi.pa165.core.service.airplane.AirplaneService;
import cz.muni.fi.pa165.core.service.flight.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FlightFacadeImpl implements FlightFacade<Long> {

    private final FlightService flightService;
    private final AirplaneService airplaneService;
    private final FlightMapper flightMapper;

    @Autowired
    public FlightFacadeImpl(FlightService flightService, AirplaneService airplaneService, FlightMapper flightMapper) {
        this.flightService = flightService;
        this.airplaneService = airplaneService;
        this.flightMapper = flightMapper;
    }

    @Override
    public FlightDto save(NewFlightDtoRequest newFlightDtoRequest) {
        var airplane = airplaneService.findById(newFlightDtoRequest.getAirplaneId())
                .orElseThrow(ResourceNotFoundException::new);
        var entityToSave = flightMapper.toEntityFromNewRequest(newFlightDtoRequest);
        entityToSave.setAirplane(airplane);
        var savedEntity = flightService.save(entityToSave);
        return flightMapper.toDto(savedEntity);
    }

    @Override
    public Optional<FlightDto> findById(Long id) {
        var foundFlightEntity = flightService.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        var flightDto = flightMapper.toDto(foundFlightEntity);

        return Optional.of(flightDto);
    }

    @Override
    public List<FlightDto> findAll() {
        var foundFlights = flightService.findAll();

        return foundFlights
                .stream()
                .map(flightMapper::toDto)
                .toList();
    }

    @Override
    public void deleteById(Long id) {
        if (!flightService.existsById(id)) {
            throw new ResourceNotFoundException();

        }
        flightService.deleteById(id);
    }

    @Override
    public FlightDto update(Long id, NewFlightDtoRequest newFlightDtoRequest) {
        if (!flightService.existsById(id)) {
            throw new ResourceNotFoundException();

        }
        var newFlightEntity = flightMapper.toEntityFromNewRequest(newFlightDtoRequest);
        var updatedFlightEntity = flightService.update(id, newFlightEntity);

        return flightMapper.toDto(updatedFlightEntity);
    }
}
