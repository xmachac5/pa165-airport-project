package cz.muni.fi.pa165.core.service.common;

import java.util.List;
import java.util.Optional;

/**
 * BaseService for common CRUD operations
 *
 * @param <E> Entity
 * @param <K> Key
 * @author martinslovik
 */
public interface BaseService<E, K> {

    E save(E entity);

    Optional<E> findById(K id);

    List<E> findAll();

    void deleteById(K id);

    void deleteAll();

    boolean existsById(K id);
}
