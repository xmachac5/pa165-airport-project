package cz.muni.fi.pa165.core.rest;

import cz.muni.fi.pa165.core.api.AirportApi;
import cz.muni.fi.pa165.core.api.AirportApiDelegate;
import cz.muni.fi.pa165.core.facade.airport.AirportFacade;
import cz.muni.fi.pa165.core.model.AirportDto;
import cz.muni.fi.pa165.core.model.NewAirportDtoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AirportController implements AirportApiDelegate {

    private final AirportFacade<Long> airportFacade;

    @Autowired
    public AirportController(AirportFacade<Long> airportFacade) {
        this.airportFacade = airportFacade;
    }

    /**
     * POST /api/airports/{airportId}/arrivingFlights/{arrivingFlightId} : Assign arriving flight to an airport.
     *
     * @param airportId        (required)
     * @param arrivingFlightId (required)
     * @return OK (status code 201)
     * or Input data not correct (status code 400)
     * @see AirportApi#assignArrivingFlight
     */
    @Override
    public ResponseEntity<AirportDto> assignArrivingFlight(Long airportId, Long arrivingFlightId) {
        return ResponseEntity.ok(airportFacade.addArrivingFlightAssignment(airportId, arrivingFlightId));
    }

    /**
     * POST /api/airports/{airportId}/departingFlights/{departingFlightId} : Assign departing flight to an airport.
     *
     * @param airportId         (required)
     * @param departingFlightId (required)
     * @return OK (status code 201)
     * or Input data not correct (status code 400)
     * @see AirportApi#assignDepartingFlight
     */
    @Override
    public ResponseEntity<AirportDto> assignDepartingFlight(Long airportId, Long departingFlightId) {
        return ResponseEntity.ok(airportFacade.addDepartingFlightAssignment(airportId, departingFlightId));
    }

    /**
     * POST /api/airports/{airportId}/city/{cityId} : Assign the city to an airport.
     *
     * @param airportId  (required)
     * @param cityId  (required)
     * @return OK (status code 201)
     *         or Input data not correct (status code 400)
     * @see AirportApi#assignCity
     */
    @Override
    public ResponseEntity<AirportDto> assignCity(Long airportId, Long cityId) {
        return ResponseEntity.ok(airportFacade.addCityAssignment(airportId, cityId));
    }

    /**
     * POST /api/airports : Create a new airport.
     * Creates a new airport and returns it as a response.
     *
     * @param newAirportDtoRequest (required)
     * @return Created (status code 201)
     * or Input data not correct (status code 400)
     * @see AirportApi#createAirport
     */
    @Override
    public ResponseEntity<AirportDto> createAirport(NewAirportDtoRequest newAirportDtoRequest) {
        return ResponseEntity.ok(airportFacade.save(newAirportDtoRequest));
    }

    /**
     * DELETE /api/airports/{id} : Delete airport by id.
     *
     * @param id (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see AirportApi#deleteAirport
     */
    @Override
    public ResponseEntity<Void> deleteAirport(Long id) {
        airportFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * DELETE /api/airports/{airportId}/arrivingFlights/{arrivingFlightId} : Delete assignment of arriving flight to an airport.
     *
     * @param airportId        (required)
     * @param arrivingFlightId (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see AirportApi#deleteArrivingFlightAssignment
     */
    @Override
    public ResponseEntity<Void> deleteArrivingFlightAssignment(Long airportId, Long arrivingFlightId) {
        airportFacade.deleteArrivingFlightAssignment(airportId, arrivingFlightId);
        return ResponseEntity.noContent().build();
    }

    /**
     * DELETE /api/airports/{airportId}/departingFlights/{departingFlightId} : Delete assignment of departing flight to an airport.
     *
     * @param airportId         (required)
     * @param departingFlightId (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see AirportApi#deleteDepartingFlightAssignment
     */
    @Override
    public ResponseEntity<Void> deleteDepartingFlightAssignment(Long airportId, Long departingFlightId) {
        airportFacade.deleteDepartingFlightAssignment(airportId, departingFlightId);
        return ResponseEntity.noContent().build();
    }

    /**
     * DELETE /api/airports/{airportId}/city/{cityId} : Delete assignment of the city to an airport.
     *
     * @param airportId  (required)
     * @param cityId  (required)
     * @return Deleted (status code 204)
     *         or Not Found (status code 404)
     * @see AirportApi#deleteCityAssignment
     */
    @Override
    public ResponseEntity<Void> deleteCityAssignment(Long airportId, Long cityId) {
        airportFacade.deleteCityAssignment(airportId, cityId);
        return ResponseEntity.noContent().build();
    }

    /**
     * GET /api/airports/{id} : Get airport by id.
     * Returns an object representing an airport.
     *
     * @param id (required)
     * @return OK (status code 200)
     * @see AirportApi#getAirportById
     */
    @Override
    public ResponseEntity<AirportDto> getAirportById(Long id) {
        return ResponseEntity.of(airportFacade.findById(id));
    }

    /**
     * GET /api/airports : Get all airports.
     * Returns an array of objects representing airports.
     *
     * @return OK (status code 200)
     * @see AirportApi#getAllAirports
     */
    @Override
    public ResponseEntity<List<AirportDto>> getAllAirports() {
        return ResponseEntity.ok(airportFacade.findAll());
    }

    /**
     * PUT /api/airports/{id} : Update airport by id.
     * Updates a airport by id and returns it as a response.
     *
     * @param id  (required)
     * @param newAirportDtoRequest  (required)
     * @return OK (status code 200)
     *         or Input data not correct (status code 400)
     * @see AirportApi#updateAirport
     */
    @Override
    public ResponseEntity<AirportDto> updateAirport(Long id, NewAirportDtoRequest newAirportDtoRequest) {
        return ResponseEntity.ok(airportFacade.update(id, newAirportDtoRequest));
    }
}
