package cz.muni.fi.pa165.core.facade.steward;

import cz.muni.fi.pa165.core.model.NewStewardDtoRequest;
import cz.muni.fi.pa165.core.model.StewardDto;

import java.util.List;
import java.util.Optional;

/**
 * @param <K> Key
 * @author martinslovik
 */
public interface StewardFacade<K> {

    StewardDto save(NewStewardDtoRequest newStewardDtoRequest);

    Optional<StewardDto> findById(K id);

    List<StewardDto> findAll();

    void deleteById(K id);

    StewardDto update(Long id, NewStewardDtoRequest newStewardDtoRequest);

    StewardDto assignStewardFlight(Long stewardId, Long flightId);

    void deleteStewardFlightsAssignment(Long stewardId, Long flightId);
}
