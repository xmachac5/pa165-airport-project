package cz.muni.fi.pa165.core.data.domain;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "cities")
@Data
@NoArgsConstructor
public class City extends DomainEntity {

    @NotNull
    @NotBlank
    @Column(unique = true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
    private List<Airport> airports = new ArrayList<>();

    public void addAirport(Airport airport) {
        airports.add(airport);
    }

    public void removeAirport(Airport airport) {
        airports.remove(airport);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof City city)) {
            return false;
        }
        return getName().equals(city.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
