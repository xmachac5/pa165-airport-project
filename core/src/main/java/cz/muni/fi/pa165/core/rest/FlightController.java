package cz.muni.fi.pa165.core.rest;

import cz.muni.fi.pa165.core.api.FlightApi;
import cz.muni.fi.pa165.core.api.FlightApiDelegate;
import cz.muni.fi.pa165.core.facade.flight.FlightFacade;
import cz.muni.fi.pa165.core.model.FlightDto;
import cz.muni.fi.pa165.core.model.NewFlightDtoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FlightController implements FlightApiDelegate {

    private final FlightFacade<Long> flightFacade;

    @Autowired
    public FlightController(FlightFacade<Long> flightFacade) {
        this.flightFacade = flightFacade;
    }

    /**
     * POST /api/flights : Create a new flight.
     * Creates a new flight and returns it as a response.
     *
     * @param newFlightDtoRequest (required)
     * @return Created (status code 201)
     * or Input data not correct (status code 400)
     * @see FlightApi#createFlight
     */
    @Override
    public ResponseEntity<FlightDto> createFlight(NewFlightDtoRequest newFlightDtoRequest) {
        return ResponseEntity.ok(flightFacade.save(newFlightDtoRequest));
    }

    /**
     * DELETE /api/flights/{id} : Delete flight by id.
     *
     * @param id (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see FlightApi#deleteFlight
     */
    @Override
    public ResponseEntity<Void> deleteFlight(Long id) {
        flightFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * GET /api/flights : Get all flights.
     * Returns an array of objects representing flights.
     *
     * @return OK (status code 200)
     * @see FlightApi#getAllFlights
     */
    @Override
    public ResponseEntity<List<FlightDto>> getAllFlights() {
        return ResponseEntity.ok(flightFacade.findAll());
    }

    /**
     * GET /api/flights/{id} : Get flight by id.
     * Returns an object representing a flight.
     *
     * @param id (required)
     * @return OK (status code 200)
     * @see FlightApi#getFlightById
     */
    @Override
    public ResponseEntity<FlightDto> getFlightById(Long id) {
        return ResponseEntity.of(flightFacade.findById(id));
    }

    /**
     * PUT /api/flights/{id} : Update flight by id.
     * Updates a flight by id and returns it as a response.
     *
     * @param id                  (required)
     * @param newFlightDtoRequest (required)
     * @return OK (status code 200)
     * or Input data not correct (status code 400)
     * @see FlightApi#updateFlight
     */
    @Override
    public ResponseEntity<FlightDto> updateFlight(Long id, NewFlightDtoRequest newFlightDtoRequest) {
        return ResponseEntity.ok(flightFacade.update(id, newFlightDtoRequest));
    }
}
