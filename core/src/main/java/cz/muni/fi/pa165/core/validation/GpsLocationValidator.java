package cz.muni.fi.pa165.core.validation;

import cz.muni.fi.pa165.core.exceptions.InvalidGpsLocationException;
import org.springframework.stereotype.Component;

@Component
public class GpsLocationValidator {

    private static final double MIN_LATITUDE = -90.0;
    private static final double MAX_LATITUDE = 90.0;
    private static final double MIN_LONGITUDE = -180.0;
    private static final double MAX_LONGITUDE = 180.0;

    public void validate(double latitude, double longitude) {
        if (!validLatitude(latitude)) {
            throw new InvalidGpsLocationException(
                    formatExceptionMessage("latitude", MIN_LATITUDE, MAX_LATITUDE, latitude)
            );
        }
        if (!validLongitude(longitude)) {
            throw new InvalidGpsLocationException(
                    formatExceptionMessage("longitude", MIN_LONGITUDE, MAX_LONGITUDE, longitude)
            );
        }
    }

    private static boolean validLatitude(double latitude) {
        return MIN_LATITUDE <= latitude && latitude <= MAX_LATITUDE;
    }

    private static boolean validLongitude(double longitude) {
        return MIN_LONGITUDE <= longitude && longitude <= MAX_LONGITUDE;
    }

    private String formatExceptionMessage(String indicator,
                                          double lowerBound,
                                          double upperBound,
                                          double indicatorsValue) {
        return String.format("Invalid value of %s given: expecting value to be from range [%.2f, %.2f], but got %.2f"
                .formatted(indicator, lowerBound, upperBound, indicatorsValue));
    }
}
