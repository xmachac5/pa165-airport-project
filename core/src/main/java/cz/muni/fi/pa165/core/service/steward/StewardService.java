package cz.muni.fi.pa165.core.service.steward;

import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.data.domain.FlightSteward;
import cz.muni.fi.pa165.core.data.domain.Steward;
import cz.muni.fi.pa165.core.service.common.BaseService;

import java.util.Optional;

public interface StewardService extends BaseService<Steward, Long> {

    /**
     * Saves the assignment of Steward to a Flight
     * @param flightSteward Flight<->Steward connection table
     * @return FlightSteward connection table stored in DB
     */
    FlightSteward saveFlightStewards(FlightSteward flightSteward);

    void deleteFlightStewards(Steward steward, Flight flight);

    Optional<Steward> findByIdWithFlights(Long id);

    Steward update(Long id, Steward newSteward);
}
