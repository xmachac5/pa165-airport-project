package cz.muni.fi.pa165.core.data.domain;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "flights")
@Data
@NoArgsConstructor
public class Flight extends DomainEntity {

    @NotNull
    @Future
    private OffsetDateTime departureTime;

    @NotNull
    @Future
    private OffsetDateTime arrivalTime;

    @OneToMany(mappedBy = "flight")
    private List<FlightSteward> flightStewards;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departing_airport_id")
    private Airport departingAirport;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "arriving_airport_id")
    private Airport arrivingAirport;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "airplane_id")
    private Airplane airplane;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Flight flight)) {
            return false;
        }
        return Objects.equals(getDepartureTime(), flight.getDepartureTime()) &&
                Objects.equals(getArrivalTime(), flight.getArrivalTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartureTime(), getArrivalTime());
    }
}
