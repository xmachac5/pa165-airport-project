package cz.muni.fi.pa165.core.service.city;

import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.service.common.BaseService;

import java.util.Optional;

public interface CityService extends BaseService<City, Long> {

    Optional<City> findByName(String name);

    City update(Long id, City updatedCity);
}
