package cz.muni.fi.pa165.core.service.airplanetype;

import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.data.repository.airplanetype.AirplaneTypeRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AirplaneTypeServiceImpl extends BaseServiceImpl<AirplaneType, Long> implements AirplaneTypeService {

    private final AirplaneTypeRepository airplaneTypeRepository;

    @Autowired
    public AirplaneTypeServiceImpl(AirplaneTypeRepository airplaneTypeRepository) {
        super(airplaneTypeRepository);
        this.airplaneTypeRepository = airplaneTypeRepository;
    }

    @Override
    public Optional<AirplaneType> findByName(String name) {
        return airplaneTypeRepository.findByName(name);
    }

    @Override
    public AirplaneType update(Long id, AirplaneType newAirplaneType) {
        var entityToUpdate = airplaneTypeRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        entityToUpdate.setName(newAirplaneType.getName());

        return airplaneTypeRepository.save(entityToUpdate);
    }
}