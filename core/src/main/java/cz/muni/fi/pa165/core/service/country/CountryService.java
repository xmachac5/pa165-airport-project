package cz.muni.fi.pa165.core.service.country;

import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.service.common.BaseService;

import java.util.Optional;

public interface CountryService extends BaseService<Country, Long> {

    Optional<Country> findByName(String name);

    Country update(Long id, Country updatedCountry);
}
