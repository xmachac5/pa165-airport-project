package cz.muni.fi.pa165.core.data.repository.airport;

import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AirportRepository extends BaseRepository<Airport, Long> {

    @Query("SELECT a FROM Airport a WHERE a.name = :name")
    Optional<Airport> findByName(@Param("name") String name);

    @Query("SELECT a FROM Airport a WHERE a.code = :code")
    Optional<Airport> findByCode(@Param("code") String code);

    @Query("SELECT a FROM Airport a WHERE a.city = :city")
    List<Airport> findByCity(@Param("city") City city);
}
