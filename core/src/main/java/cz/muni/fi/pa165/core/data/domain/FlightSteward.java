package cz.muni.fi.pa165.core.data.domain;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Objects;

/**
 * Connection table for flight-steward many-to-many relationship
 */
@Entity(name = "flight_steward")
@Data
@NoArgsConstructor
public class FlightSteward extends DomainEntity {

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Flight flight;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Steward steward;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FlightSteward flightSteward)) {
            return false;
        }
        return Objects.equals(getFlight(), flightSteward.getFlight()) &&
                Objects.equals(getSteward(), flightSteward.getSteward());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlight(), getSteward());
    }
}
