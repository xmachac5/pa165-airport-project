package cz.muni.fi.pa165.core.data.repository.country;

import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CountryRepository extends BaseRepository<Country, Long> {

    @Query("SELECT c FROM Country c WHERE c.name = :name")
    Optional<Country> findByName(@Param("name") String name);
}
