package cz.muni.fi.pa165.core.rest;

import cz.muni.fi.pa165.core.api.AirplaneTypeApi;
import cz.muni.fi.pa165.core.api.AirplaneTypeApiDelegate;
import cz.muni.fi.pa165.core.facade.airplanetype.AirplaneTypeFacade;
import cz.muni.fi.pa165.core.model.AirplaneTypeDto;
import cz.muni.fi.pa165.core.model.NewAirplaneTypeDtoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AirplaneTypeController implements AirplaneTypeApiDelegate {

    private final AirplaneTypeFacade<Long> airplaneTypeFacade;

    @Autowired
    public AirplaneTypeController(AirplaneTypeFacade<Long> airplaneTypeFacade) {
        this.airplaneTypeFacade = airplaneTypeFacade;
    }

    /**
     * POST /api/airplaneTypes : Create a new airplane type.
     * Creates a new airplane type and returns it as a response.
     *
     * @param newAirplaneTypeDtoRequest (required)
     * @return Created (status code 201)
     * or Input data not correct (status code 400)
     * @see AirplaneTypeApi#createAirplaneType
     */
    @Override
    public ResponseEntity<AirplaneTypeDto> createAirplaneType(NewAirplaneTypeDtoRequest newAirplaneTypeDtoRequest) {
        return ResponseEntity.ok(airplaneTypeFacade.save(newAirplaneTypeDtoRequest));
    }

    /**
     * DELETE /api/airplaneTypes/{id} : Delete airplane type by id.
     *
     * @param id (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see AirplaneTypeApi#deleteAirplaneType
     */
    @Override
    public ResponseEntity<Void> deleteAirplaneType(Long id) {
        airplaneTypeFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * GET /api/airplaneTypes/{id} : Get airplane type by id.
     * Returns an object representing an airplane type.
     *
     * @param id (required)
     * @return OK (status code 200)
     * @see AirplaneTypeApi#getAirplaneTypeById
     */
    @Override
    public ResponseEntity<AirplaneTypeDto> getAirplaneTypeById(Long id) {
        return ResponseEntity.of(airplaneTypeFacade.findById(id));
    }

    /**
     * GET /api/airplaneTypes : Get all airplane types.
     * Returns an array of objects representing airplane types.
     *
     * @return OK (status code 200)
     * @see AirplaneTypeApi#getAllAirplaneTypes
     */
    @Override
    public ResponseEntity<List<AirplaneTypeDto>> getAllAirplaneTypes() {
        return ResponseEntity.ok(airplaneTypeFacade.findAll());
    }

    /**
     * PUT /api/airplaneTypes/{id} : Update airplane type by id.
     * Updates a airplane type by id and returns it as a response.
     *
     * @param id              (required)
     * @param newAirplaneTypeDtoRequest (required)
     * @return OK (status code 200)
     * or Input data not correct (status code 400)
     * @see AirplaneTypeApi#updateAirplaneType
     */
    @Override
    public ResponseEntity<AirplaneTypeDto> updateAirplaneType(Long id, NewAirplaneTypeDtoRequest newAirplaneTypeDtoRequest) {
        return ResponseEntity.ok(airplaneTypeFacade.update(id, newAirplaneTypeDtoRequest));
    }
}
