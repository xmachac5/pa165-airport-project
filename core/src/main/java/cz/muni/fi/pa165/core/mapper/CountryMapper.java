package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.model.CountryDto;
import cz.muni.fi.pa165.core.model.NewCountryDtoRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CountryMapper {

    Country toEntityFromNewRequest(NewCountryDtoRequest newCountryDtoRequest);

    @Mapping(target = "citiesIds", source = "cities")
    CountryDto toDto(Country country);

    default List<Long> mapCitiesToIds(List<City> cities) {
        return cities
                .stream()
                .map(City::getId)
                .toList();
    }
}
