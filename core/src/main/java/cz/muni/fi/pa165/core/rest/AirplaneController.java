package cz.muni.fi.pa165.core.rest;

import cz.muni.fi.pa165.core.api.AirplaneApi;
import cz.muni.fi.pa165.core.api.AirplaneApiDelegate;
import cz.muni.fi.pa165.core.facade.airplane.AirplaneFacade;
import cz.muni.fi.pa165.core.model.AirplaneDto;
import cz.muni.fi.pa165.core.model.NewAirplaneDtoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AirplaneController implements AirplaneApiDelegate {

    private final AirplaneFacade<Long> airplaneFacade;

    @Autowired
    public AirplaneController(AirplaneFacade<Long> airplaneFacade) {
        this.airplaneFacade = airplaneFacade;
    }


    /**
     * POST /api/airplanes : Create a new airplane.
     * Creates a new airplane and returns it as a response.
     *
     * @param newAirplaneDtoRequest (required)
     * @return Created (status code 201)
     * or Input data not correct (status code 400)
     * @see AirplaneApi#createAirplane
     */
    @Override
    public ResponseEntity<AirplaneDto> createAirplane(NewAirplaneDtoRequest newAirplaneDtoRequest) {
        return ResponseEntity.ok(airplaneFacade.save(newAirplaneDtoRequest));
    }

    /**
     * DELETE /api/airplanes/{id} : Delete airplane by id.
     *
     * @param id (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see AirplaneApi#deleteAirplane
     */
    @Override
    public ResponseEntity<Void> deleteAirplane(Long id) {
        airplaneFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }


    /**
     * GET /api/airplanes/{id} : Get airplane by id.
     * Returns an object representing an airplane.
     *
     * @param id (required)
     * @return OK (status code 200)
     * @see AirplaneApi#getAirplaneById
     */
    @Override
    public ResponseEntity<AirplaneDto> getAirplaneById(Long id) {
        return ResponseEntity.of(airplaneFacade.findById(id));
    }

    /**
     * GET /api/airplanes : Get all airplanes.
     * Returns an array of objects representing airplanes.
     *
     * @return OK (status code 200)
     * @see AirplaneApi#getAllAirplanes
     */
    @Override
    public ResponseEntity<List<AirplaneDto>> getAllAirplanes() {
        return ResponseEntity.ok(airplaneFacade.findAll());
    }

    /**
     * PUT /api/airplanes/{id} : Update airplane by id.
     * Updates a airplane by id and returns it as a response.
     *
     * @param id          (required)
     * @param newAirplaneDtoRequest (required)
     * @return OK (status code 200)
     * or Input data not correct (status code 400)
     * or Input data not correct (status code 400)
     * @see AirplaneApi#updateAirplane
     */
    @Override
    public ResponseEntity<AirplaneDto> updateAirplane(Long id, NewAirplaneDtoRequest newAirplaneDtoRequest) {
        return ResponseEntity.ok(airplaneFacade.update(id, newAirplaneDtoRequest));
    }
}
