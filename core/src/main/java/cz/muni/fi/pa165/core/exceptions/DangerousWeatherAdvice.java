package cz.muni.fi.pa165.core.exceptions;

import cz.muni.fi.pa165.core.model.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.OffsetDateTime;

@ControllerAdvice
public class DangerousWeatherAdvice {

    @ExceptionHandler(DangerousWeatherException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    ResponseEntity<ErrorMessage> handleDangerousWeatherException(DangerousWeatherException ex) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setTimestamp(OffsetDateTime.now());
        errorMessage.setStatus(HttpStatus.PRECONDITION_FAILED.value());
        errorMessage.setError(HttpStatus.PRECONDITION_FAILED.getReasonPhrase());
        errorMessage.setMessage(ex.getMessage());
        errorMessage.setPath(ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString());
        return new ResponseEntity<>(errorMessage, HttpStatus.PRECONDITION_FAILED);
    }
}
