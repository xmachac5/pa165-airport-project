package cz.muni.fi.pa165.core.data.repository.steward;

import cz.muni.fi.pa165.core.data.domain.Steward;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StewardRepository extends BaseRepository<Steward, Long> {

    @Query("SELECT s FROM Steward s JOIN FETCH s.flightStewards fs WHERE s.id = :id")
    Optional<Steward> findByIdWithFlights(@Param("id") Long id);
}
