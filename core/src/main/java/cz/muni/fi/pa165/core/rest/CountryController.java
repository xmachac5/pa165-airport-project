package cz.muni.fi.pa165.core.rest;

import cz.muni.fi.pa165.core.api.CountryApi;
import cz.muni.fi.pa165.core.api.CountryApiDelegate;
import cz.muni.fi.pa165.core.facade.country.CountryFacade;
import cz.muni.fi.pa165.core.model.CountryDto;
import cz.muni.fi.pa165.core.model.NewCountryDtoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CountryController implements CountryApiDelegate {

    private final CountryFacade<Long> countryFacade;

    @Autowired
    public CountryController(CountryFacade<Long> countryFacade) {
        this.countryFacade = countryFacade;
    }

    /**
     * POST /api/countries : Create a new country.
     * Creates a new country and returns it as a response.
     *
     * @param newCountryDtoRequest (required)
     * @return Created (status code 201)
     * or Input data not correct (status code 400)
     * @see CountryApi#createCountry
     */
    @Override
    public ResponseEntity<CountryDto> createCountry(NewCountryDtoRequest newCountryDtoRequest) {
        return ResponseEntity.ok(countryFacade.save(newCountryDtoRequest));
    }

    /**
     * DELETE /api/countries/{id} : Delete country by id.
     *
     * @param id (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see CountryApi#deleteCountry
     */
    @Override
    public ResponseEntity<Void> deleteCountry(Long id) {
        countryFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * GET /api/countries : Get all countries.
     * Returns an array of objects representing countries.
     *
     * @return OK (status code 200)
     * @see CountryApi#getAllCountries
     */
    @Override
    public ResponseEntity<List<CountryDto>> getAllCountries() {
        return ResponseEntity.ok(countryFacade.findAll());
    }

    /**
     * GET /api/countries/{id} : Get country by id.
     * Returns an object representing a country.
     *
     * @param id (required)
     * @return OK (status code 200)
     * @see CountryApi#getCountryById
     */
    @Override
    public ResponseEntity<CountryDto> getCountryById(Long id) {
        return ResponseEntity.of(countryFacade.findById(id));
    }

    /**
     * PUT /api/countries/{id} : Update country by id.
     * Updates a country by id and returns it as a response.
     *
     * @param id  (required)
     * @param newCountryDtoRequest  (required)
     * @return OK (status code 200)
     *         or Input data not correct (status code 400)
     * @see CountryApi#updateCountry
     */
    @Override
    public ResponseEntity<CountryDto> updateCountry(Long id, NewCountryDtoRequest newCountryDtoRequest) {
        return ResponseEntity.ok(countryFacade.update(id, newCountryDtoRequest));
    }
}
