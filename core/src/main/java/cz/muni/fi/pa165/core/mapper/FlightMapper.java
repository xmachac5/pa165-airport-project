package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.data.domain.FlightSteward;
import cz.muni.fi.pa165.core.model.FlightDto;
import cz.muni.fi.pa165.core.model.NewFlightDtoRequest;
import org.mapstruct.*;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface FlightMapper {

    Flight toEntityFromNewRequest(NewFlightDtoRequest newFlightDtoRequest);

    default List<Long> toStewardIdList(List<FlightSteward> flightStewards) {
        if (flightStewards != null) {
            return flightStewards.stream()
                    .map(fs -> fs.getSteward().getId())
                    .toList();
        }
        return new ArrayList<>();
    }

    default Long toAirplaneId(Airplane airplane) {
        return airplane == null ? 0L : airplane.getId();
    }

    default Long toAirportId(Airport airport) {
        return airport == null ? 0L : airport.getId();
    }

    @Mapping(target = "assignedStewardIds", expression = "java(toStewardIdList(flight.getFlightStewards()))")
    @Mapping(target = "airplaneId", expression = "java(toAirplaneId(flight.getAirplane()))")
    @Mapping(target = "departureAirportId", expression = "java(toAirportId(flight.getDepartingAirport()))")
    @Mapping(target = "arrivalAirportId", expression = "java(toAirportId(flight.getArrivingAirport()))")
    FlightDto toDto(Flight flight);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Flight partialUpdate(FlightDto flightDto, @MappingTarget Flight flight);
}
