package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.model.CityDto;
import cz.muni.fi.pa165.core.model.NewCityDtoRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CityMapper {

    City toEntityFromNewRequest(NewCityDtoRequest newCityDtoRequest);

    @Mapping(target = "countryId", source = "country")
    @Mapping(target = "airportsIds", source = "airports")
    CityDto toDto(City city);

    default Long getCountryId(Country country) {
        return country == null ? 0L : country.getId();
    }

    default List<Long> mapAirportsToIds(List<Airport> airports) {
        return airports
                .stream()
                .map(Airport::getId)
                .toList();
    }
}
