package cz.muni.fi.pa165.core.facade.country;

import cz.muni.fi.pa165.core.model.CountryDto;
import cz.muni.fi.pa165.core.model.NewCountryDtoRequest;

import java.util.List;
import java.util.Optional;

/**
 * @param <K> Key
 */
public interface CountryFacade<K> {

    CountryDto save(NewCountryDtoRequest newCountryDtoRequest);

    Optional<CountryDto> findById(K id);

    List<CountryDto> findAll();

    void deleteById(K id);

    void deleteAll();

    CountryDto update(K id, NewCountryDtoRequest newCountryDtoRequest);

    Optional<CountryDto> findByName(String name);
}
