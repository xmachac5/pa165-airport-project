package cz.muni.fi.pa165.core.data.repository.common;

import cz.muni.fi.pa165.core.data.domain.common.DomainEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import java.util.Optional;

/**
 * BaseRepository for common CRUD operations
 *
 * @param <E> Entity
 * @param <K> Key
 * @author martinslovik
 */
@Repository
@Validated
public interface BaseRepository<E extends DomainEntity, K> extends CrudRepository<E, K> {

    <S extends E> S save(S entity);

    <S extends E> Iterable<S> saveAll(Iterable<S> entities);

    Optional<E> findById(K id);

    boolean existsById(K id);

    Iterable<E> findAll();

    Iterable<E> findAllById(Iterable<K> ids);

    long count();

    void deleteById(K id);

    void delete(E entity);

    void deleteAllById(Iterable<? extends K> ids);

    void deleteAll(Iterable<? extends E> entities);

    void deleteAll();
}
