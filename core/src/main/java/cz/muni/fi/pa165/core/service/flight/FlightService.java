package cz.muni.fi.pa165.core.service.flight;

import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.service.common.BaseService;

import java.util.Optional;

public interface FlightService extends BaseService<Flight, Long> {

    Optional<Flight> findByIdWithStewards(Long id);

    Flight update(Long id, Flight newFlight);
}
