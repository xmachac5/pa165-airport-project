package cz.muni.fi.pa165.core.service.airplane;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.data.repository.airplane.AirplaneRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AirplaneServiceImpl extends BaseServiceImpl<Airplane, Long> implements AirplaneService {

    private final AirplaneRepository airplaneRepository;

    @Autowired
    public AirplaneServiceImpl(AirplaneRepository airplaneRepository) {
        super(airplaneRepository);
        this.airplaneRepository = airplaneRepository;
    }

    @Override
    public Airplane update(Long id, Airplane newAirplane) {
        var entityToUpdate = airplaneRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        entityToUpdate.setName(newAirplane.getName());
        entityToUpdate.setCapacity(newAirplane.getCapacity());

        return airplaneRepository.save(entityToUpdate);
    }
}
