package cz.muni.fi.pa165.core.data.repository.flightsteward;

import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.data.domain.FlightSteward;
import cz.muni.fi.pa165.core.data.domain.Steward;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FlightStewardRepository extends BaseRepository<FlightSteward, Long> {

    Optional<FlightSteward> findByStewardAndFlight(Steward steward, Flight flight);
}
