package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.model.AirportDto;
import cz.muni.fi.pa165.core.model.NewAirportDtoRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public abstract class AirportMapper {

    @Mapping(target = "latitude", source = "location.latitude")
    @Mapping(target = "longitude", source = "location.longitude")
    public abstract Airport toEntityFromNewRequest(NewAirportDtoRequest newAirportDtoRequest);

    @Mapping(target = "cityId", source = "city")
    @Mapping(target = "arrivingFlightsIds", source = "arrivingFlights")
    @Mapping(target = "departingFlightsIds", source = "departingFlights")
    @Mapping(target = "location.latitude", source = "latitude")
    @Mapping(target = "location.longitude", source = "longitude")
    public abstract AirportDto toDto(Airport airport);

    protected Long getCityId(City city) {
        return city == null ? 0L : city.getId();
    }

    protected List<Long> mapFlightsToIds(List<Flight> flights) {
        return flights
                .stream()
                .map(Flight::getId)
                .toList();
    }
}
