package cz.muni.fi.pa165.core.exceptions;

/**
 * Thrown in case of invalid GPS location, e.g. (-120.3, 42.42).
 */
public class InvalidGpsLocationException extends RuntimeException {

    public InvalidGpsLocationException(String message) {
        super(message);
    }
}
