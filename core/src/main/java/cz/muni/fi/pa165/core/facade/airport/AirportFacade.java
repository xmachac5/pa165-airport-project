package cz.muni.fi.pa165.core.facade.airport;

import cz.muni.fi.pa165.core.model.AirportDto;
import cz.muni.fi.pa165.core.model.NewAirportDtoRequest;

import java.util.List;
import java.util.Optional;

/**
 * @param <K> Key
 */
public interface AirportFacade<K> {

    AirportDto save(NewAirportDtoRequest newAirportDtoRequest);

    Optional<AirportDto> findById(K id);

    List<AirportDto> findAll();

    void deleteById(K id);

    void deleteAll();

    AirportDto update(K id, NewAirportDtoRequest newAirportDtoRequest);

    Optional<AirportDto> findByName(String name);

    Optional<AirportDto> findByCode(String code);

    List<AirportDto> findByCity(K cityId);

    AirportDto addArrivingFlightAssignment(K airportId, K flightId);

    AirportDto deleteArrivingFlightAssignment(K airportId, K flightId);

    AirportDto addDepartingFlightAssignment(K airportId, K flightId);

    AirportDto deleteDepartingFlightAssignment(K airportId, K flightId);

    AirportDto addCityAssignment(K airportId, K cityId);

    AirportDto deleteCityAssignment(K airportId, K cityId);
}
