package cz.muni.fi.pa165.core.service.airport;

import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.repository.airport.AirportRepository;
import cz.muni.fi.pa165.core.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AirportServiceImpl extends BaseServiceImpl<Airport, Long> implements AirportService {

    private final AirportRepository airportRepository;

    @Autowired
    public AirportServiceImpl(AirportRepository airportRepository) {
        super(airportRepository);
        this.airportRepository = airportRepository;
    }

    @Override
    public Optional<Airport> findByName(String name) {
        return airportRepository.findByName(name);
    }

    @Override
    public Optional<Airport> findByCode(String code) {
        return airportRepository.findByCode(code);
    }

    @Override
    public List<Airport> findByCity(City city) {
        return airportRepository.findByCity(city);
    }

    @Override
    public Airport update(Long id, Airport updatedAirport) {
        Airport airportToUpdate = airportRepository.findById(id)
                .orElseThrow(RuntimeException::new);

        airportToUpdate.setName(updatedAirport.getName());
        airportToUpdate.setCode(updatedAirport.getCode());

        return airportRepository.save(airportToUpdate);
    }
}
