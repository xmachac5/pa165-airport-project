package cz.muni.fi.pa165.core.facade.country;

import cz.muni.fi.pa165.core.data.domain.Country;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.mapper.CountryMapper;
import cz.muni.fi.pa165.core.model.CountryDto;
import cz.muni.fi.pa165.core.model.NewCountryDtoRequest;
import cz.muni.fi.pa165.core.service.country.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountryFacadeImpl implements CountryFacade<Long> {

    private final CountryService countryService;
    private final CountryMapper countryMapper;

    @Autowired
    public CountryFacadeImpl(CountryService countryService, CountryMapper countryMapper) {
        this.countryService = countryService;
        this.countryMapper = countryMapper;
    }

    @Override
    public CountryDto save(NewCountryDtoRequest newCountryDtoRequest) {
        Country entityToSave = countryMapper.toEntityFromNewRequest(newCountryDtoRequest);
        Country savedEntity = countryService.save(entityToSave);
        return countryMapper.toDto(savedEntity);
    }

    @Override
    public Optional<CountryDto> findById(Long id) {
        Country foundEntity = countryService.findById(id)
                .orElseThrow(ResourceNotFoundException::new);
        return Optional.of(countryMapper.toDto(foundEntity));
    }

    @Override
    public List<CountryDto> findAll() {
        List<Country> foundEntities = countryService.findAll();
        return foundEntities.stream()
                .map(countryMapper::toDto)
                .toList();
    }

    @Override
    public void deleteById(Long id) {
        if (!countryService.existsById(id)) {
            throw new ResourceNotFoundException();
        }
        countryService.deleteById(id);
    }

    @Override
    public void deleteAll() {
        countryService.deleteAll();
    }

    @Override
    public CountryDto update(Long id, NewCountryDtoRequest newCountryDtoRequest) {
        if (!countryService.existsById(id)) {
            throw new ResourceNotFoundException();
        }

        Country newEntity = countryMapper.toEntityFromNewRequest(newCountryDtoRequest);
        Country updatedEntity = countryService.update(id, newEntity);
        return countryMapper.toDto(updatedEntity);
    }

    @Override
    public Optional<CountryDto> findByName(String name) {
        Optional<Country> foundEntity = countryService.findByName(name);
        return foundEntity.map(countryMapper::toDto);
    }
}
