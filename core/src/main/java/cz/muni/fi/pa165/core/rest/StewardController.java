package cz.muni.fi.pa165.core.rest;

import cz.muni.fi.pa165.core.api.StewardApi;
import cz.muni.fi.pa165.core.api.StewardApiDelegate;
import cz.muni.fi.pa165.core.facade.steward.StewardFacade;
import cz.muni.fi.pa165.core.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StewardController implements StewardApiDelegate {

    private final StewardFacade<Long> stewardFacade;

    @Autowired
    public StewardController(StewardFacade<Long> stewardFacade) {
        this.stewardFacade = stewardFacade;
    }

    /**
     * POST /api/stewards : Create a new steward.
     * Creates a new steward and returns it as a response.
     *
     * @param newStewardDtoRequest (required)
     * @return Response containing a single Steward. (status code 201)
     * or Input data not correct (status code 400)
     * @see StewardApi#createSteward
     */
    @Override
    public ResponseEntity<StewardDto> createSteward(NewStewardDtoRequest newStewardDtoRequest) {
        return ResponseEntity.ok(stewardFacade.save(newStewardDtoRequest));
    }

    /**
     * POST /api/stewards/{stewardId}/flights/{flightId} : Assign steward to a flight.
     *
     * @param stewardId (required)
     * @param flightId  (required)
     * @return Response containing a single Steward. (status code 201)
     * or Input data not correct (status code 400)
     * @see StewardApi#createStewardFlights
     */
    @Override
    public ResponseEntity<StewardDto> createStewardFlights(Long stewardId, Long flightId) {
        return ResponseEntity.ok(stewardFacade.assignStewardFlight(stewardId, flightId));
    }

    /**
     * DELETE /api/stewards/{id} : Delete steward by id.
     *
     * @param id (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see StewardApi#deleteSteward
     */
    @Override
    public ResponseEntity<Void> deleteSteward(Long id) {
        stewardFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * DELETE /api/stewards/{stewardId}/flights/{flightId} : Delete assignment of steward to a flight.
     *
     * @param stewardId (required)
     * @param flightId  (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see StewardApi#deleteStewardFlights
     */
    @Override
    public ResponseEntity<Void> deleteStewardFlights(Long stewardId, Long flightId) {
        stewardFacade.deleteStewardFlightsAssignment(stewardId, flightId);
        return ResponseEntity.noContent().build();
    }

    /**
     * GET /api/stewards : Get all stewards.
     * Returns an array of objects representing stewards.
     *
     * @return OK (status code 200)
     * @see StewardApi#getAllStewards
     */
    @Override
    public ResponseEntity<List<StewardDto>> getAllStewards() {
        return ResponseEntity.ok(stewardFacade.findAll());
    }

    /**
     * GET /api/stewards/{id} : Get steward by id
     * Returns a steward by id.
     *
     * @param id (required)
     * @return OK (status code 200)
     * or Not Found (status code 404)
     * @see StewardApi#getSteward
     */
    @Override
    public ResponseEntity<StewardDto> getSteward(Long id) {
        return ResponseEntity.of(stewardFacade.findById(id));
    }

    /**
     * GET /api/stewards/paged : Paged stewards
     * Returns a page of stewards. The parameter &#x60;page&#x60; specifies zero-based index of the requested page, and the parameter &#x60;size&#x60; specifies the size of the page.
     *
     * @param page Zero-based page index (0..N) (optional, default to 0)
     * @param size The size of the page to be returned (optional, default to 20)
     * @param sort Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported. (optional)
     * @return OK (status code 200)
     * @see StewardApi#getStewardsPaged
     */
    @Override
    public ResponseEntity<PageStewardDto> getStewardsPaged(Integer page, Integer size, List<String> sort) {
        return StewardApiDelegate.super.getStewardsPaged(page, size, sort);
    }

    /**
     * PUT /api/stewards/{id} : Update steward by id.
     * Updates a steward by id and returns it as a response.
     *
     * @param id                   (required)
     * @param newStewardDtoRequest (required)
     * @return Response containing a single Steward. (status code 200)
     * or Input data not correct (status code 400)
     * @see StewardApi#updateSteward
     */
    @Override
    public ResponseEntity<StewardDto> updateSteward(Long id, NewStewardDtoRequest newStewardDtoRequest) {
        return ResponseEntity.ok(stewardFacade.update(id, newStewardDtoRequest));
    }
}
