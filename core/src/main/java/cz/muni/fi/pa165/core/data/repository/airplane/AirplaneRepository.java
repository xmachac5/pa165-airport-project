package cz.muni.fi.pa165.core.data.repository.airplane;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirplaneRepository extends BaseRepository<Airplane, Long> {
    
}
