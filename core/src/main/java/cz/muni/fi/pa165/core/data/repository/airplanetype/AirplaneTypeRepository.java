package cz.muni.fi.pa165.core.data.repository.airplanetype;

import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.data.repository.common.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AirplaneTypeRepository extends BaseRepository<AirplaneType, Long> {

    Optional<AirplaneType> findByName(String name);
}
