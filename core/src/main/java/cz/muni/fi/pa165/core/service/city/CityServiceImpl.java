package cz.muni.fi.pa165.core.service.city;

import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.repository.city.CityRepository;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.service.common.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CityServiceImpl extends BaseServiceImpl<City, Long> implements CityService {

    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        super(cityRepository);
        this.cityRepository = cityRepository;
    }

    @Override
    public Optional<City> findByName(String name) {
        return cityRepository.findByName(name);
    }

    @Override
    public City update(Long id, City updatedCity) {
        City cityToUpdate = cityRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        cityToUpdate.setName(updatedCity.getName());

        return cityRepository.save(cityToUpdate);
    }
}
