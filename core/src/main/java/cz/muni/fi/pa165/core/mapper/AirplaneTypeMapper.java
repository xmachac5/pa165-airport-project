package cz.muni.fi.pa165.core.mapper;

import cz.muni.fi.pa165.core.data.domain.Airplane;
import cz.muni.fi.pa165.core.data.domain.AirplaneType;
import cz.muni.fi.pa165.core.model.AirplaneTypeDto;
import cz.muni.fi.pa165.core.model.NewAirplaneTypeDtoRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AirplaneTypeMapper {

    AirplaneType toEntityFromNewRequest(NewAirplaneTypeDtoRequest newAirplaneTypeDtoRequest);

    @Mapping(target = "airplanesIds", source = "airplanes")
    AirplaneTypeDto toDto(AirplaneType airplaneType);

    default Long getAirplanesTypeId(AirplaneType airplaneType) {
        return airplaneType == null ? 0L : airplaneType.getId();
    }

    default List<Long> mapAirplaneTypesToIds(List<Airplane> airplanes) {
        return airplanes
                .stream()
                .map(Airplane::getId)
                .toList();
    }
}
