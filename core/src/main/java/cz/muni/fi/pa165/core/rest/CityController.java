package cz.muni.fi.pa165.core.rest;

import cz.muni.fi.pa165.core.api.CityApi;
import cz.muni.fi.pa165.core.api.CityApiDelegate;
import cz.muni.fi.pa165.core.facade.city.CityFacade;
import cz.muni.fi.pa165.core.model.CityDto;
import cz.muni.fi.pa165.core.model.NewCityDtoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CityController implements CityApiDelegate {

    private final CityFacade<Long> cityFacade;

    @Autowired
    public CityController(CityFacade<Long> cityFacade) {
        this.cityFacade = cityFacade;
    }

    /**
     * POST /api/cities/{cityId}/countries/{countryId} : Assign country to a city.
     *
     * @param cityId    (required)
     * @param countryId (required)
     * @return OK (status code 201)
     * or Input data not correct (status code 400)
     * @see CityApi#assignCountry
     */
    @Override
    public ResponseEntity<CityDto> assignCountry(Long cityId, Long countryId) {
        return ResponseEntity.ok(cityFacade.addCountryAssignment(cityId, countryId));
    }

    /**
     * POST /api/cities : Create a new city.
     * Creates a new city and returns it as a response.
     *
     * @param newCityDtoRequest (required)
     * @return Created (status code 201)
     * or Input data not correct (status code 400)
     * @see CityApi#createCity
     */
    @Override
    public ResponseEntity<CityDto> createCity(NewCityDtoRequest newCityDtoRequest) {
        return ResponseEntity.ok(cityFacade.save(newCityDtoRequest));
    }

    /**
     * DELETE /api/cities/{id} : Delete city by id.
     *
     * @param id (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see CityApi#deleteCity
     */
    @Override
    public ResponseEntity<Void> deleteCity(Long id) {
        cityFacade.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * DELETE /api/cities/{cityId}/countries/{countryId} : Delete assignment of country to a city.
     *
     * @param cityId    (required)
     * @param countryId (required)
     * @return Deleted (status code 204)
     * or Not Found (status code 404)
     * @see CityApi#deleteCountryAssignment
     */
    @Override
    public ResponseEntity<Void> deleteCountryAssignment(Long cityId, Long countryId) {
        cityFacade.deleteCountryAssignment(cityId, countryId);
        return ResponseEntity.noContent().build();
    }

    /**
     * GET /api/cities/{id} : Get city by id.
     * Returns an object representing a city.
     *
     * @param id (required)
     * @return OK (status code 200)
     * @see CityApi#getCityById
     */
    @Override
    public ResponseEntity<CityDto> getCityById(Long id) {
        return ResponseEntity.of(cityFacade.findById(id));
    }

    /**
     * GET /api/cities : Get all cities.
     * Returns an array of objects representing cities.
     *
     * @return OK (status code 200)
     * @see CityApi#getAllCities
     */
    @Override
    public ResponseEntity<List<CityDto>> getAllCities() {
        return ResponseEntity.ok(cityFacade.findAll());
    }

    /**
     * PUT /api/cities/{id} : Update city by id.
     * Updates a city by id and returns it as a response.
     *
     * @param id  (required)
     * @param newCityDtoRequest  (required)
     * @return OK (status code 200)
     *         or Input data not correct (status code 400)
     * @see CityApi#updateCity
     */
    @Override
    public ResponseEntity<CityDto> updateCity(Long id, NewCityDtoRequest newCityDtoRequest) {
        return ResponseEntity.ok(cityFacade.update(id, newCityDtoRequest));
    }
}
