package cz.muni.fi.pa165.core.facade.airport;

import cz.muni.chat.fi.pa165.weather.client.invoker.ApiClient;
import cz.muni.chat.fi.pa165.weather.client.invoker.ApiException;
import cz.muni.fi.pa165.core.data.domain.Airport;
import cz.muni.fi.pa165.core.data.domain.City;
import cz.muni.fi.pa165.core.data.domain.Flight;
import cz.muni.fi.pa165.core.exceptions.DangerousWeatherException;
import cz.muni.fi.pa165.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.core.mapper.AirportMapper;
import cz.muni.fi.pa165.core.model.AirportDto;
import cz.muni.fi.pa165.core.model.NewAirportDtoRequest;
import cz.muni.fi.pa165.core.service.airport.AirportService;
import cz.muni.fi.pa165.core.service.city.CityService;
import cz.muni.fi.pa165.core.service.flight.FlightService;
import cz.muni.fi.pa165.core.validation.GpsLocationValidator;
import cz.muni.fi.pa165.weather.client.WeatherApi;
import cz.muni.fi.pa165.weather.client.model.FlightCreationAdviceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AirportFacadeImpl implements AirportFacade<Long> {

    private final AirportService airportService;
    private final FlightService flightService;
    private final CityService cityService;
    private final AirportMapper airportMapper;
    private final GpsLocationValidator gpsLocationValidator;
    private final WeatherApi weatherApi = new WeatherApi(getApiClient());

    @Autowired
    public AirportFacadeImpl(
            AirportService airportService,
            FlightService flightService,
            CityService cityService,
            AirportMapper airportMapper,
            GpsLocationValidator gpsLocationValidator
    ) {
        this.airportService = airportService;
        this.flightService = flightService;
        this.cityService = cityService;
        this.airportMapper = airportMapper;
        this.gpsLocationValidator = gpsLocationValidator;
    }

    private ApiClient getApiClient() {
        var client = new ApiClient();
        client.setRequestInterceptor((builder -> {
            var authentication = SecurityContextHolder.getContext().getAuthentication();
            var token = (OAuth2AccessToken) (((BearerTokenAuthentication) authentication).getToken());
            builder.header("Authorization", "Bearer " + token.getTokenValue());
        }));
        return client;
    }

    @Override
    public AirportDto save(NewAirportDtoRequest newAirportDtoRequest) {
        gpsLocationValidator.validate(newAirportDtoRequest.getLocation().getLatitude(),
                newAirportDtoRequest.getLocation().getLongitude());

        Airport entityToSave = airportMapper.toEntityFromNewRequest(newAirportDtoRequest);
        Airport savedEntity = airportService.save(entityToSave);
        return airportMapper.toDto(savedEntity);
    }

    @Override
    public Optional<AirportDto> findById(Long id) {
        Airport foundEntity = airportService.findById(id)
                .orElseThrow(ResourceNotFoundException::new);
        return Optional.of(airportMapper.toDto(foundEntity));
    }

    @Override
    public List<AirportDto> findAll() {
        List<Airport> foundEntities = airportService.findAll();
        return foundEntities.stream()
                .map(airportMapper::toDto)
                .toList();
    }

    @Override
    public void deleteById(Long id) {
        airportService.deleteById(id);
    }

    @Override
    public void deleteAll() {
        airportService.deleteAll();
    }

    @Override
    public AirportDto update(Long id, NewAirportDtoRequest newAirportDtoRequest) {
        gpsLocationValidator.validate(newAirportDtoRequest.getLocation().getLatitude(),
                newAirportDtoRequest.getLocation().getLongitude());

        if (!airportService.existsById(id)) {
            throw new ResourceNotFoundException();
        }
        Airport newEntity = airportMapper.toEntityFromNewRequest(newAirportDtoRequest);
        Airport updatedEntity = airportService.update(id, newEntity);
        return airportMapper.toDto(updatedEntity);
    }

    @Override
    public Optional<AirportDto> findByName(String name) {
        Optional<Airport> foundEntity = airportService.findByName(name);
        return foundEntity.map(airportMapper::toDto);
    }

    @Override
    public Optional<AirportDto> findByCode(String code) {
        Optional<Airport> foundEntity = airportService.findByCode(code);
        return foundEntity.map(airportMapper::toDto);
    }

    @Override
    public List<AirportDto> findByCity(Long cityId) {
        City city = cityService.findById(cityId)
                .orElseThrow(ResourceNotFoundException::new);
        List<Airport> foundAirports = airportService.findByCity(city);
        return foundAirports.stream()
                .map(airportMapper::toDto)
                .toList();
    }

    @Override
    public AirportDto addArrivingFlightAssignment(Long airportId, Long flightId) {
        Airport airport = airportService.findById(airportId)
                .orElseThrow(ResourceNotFoundException::new);
        Flight flight = flightService.findById(flightId)
                .orElseThrow(ResourceNotFoundException::new);

        try {
            FlightCreationAdviceDto advice = weatherApi.isSafeToCreateFlight(
                    airportId,
                    airportId,
                    flight.getDepartureTime(),
                    flight.getArrivalTime()
            );
            if (!advice.getResult()) {
                informAboutDangerousWeather(advice);
            }
        } catch (ApiException ex) {
            throw new ResourceNotFoundException(ex);
        }

        airport.addArrivingFlight(flight);
        flight.setArrivingAirport(airport);

        Airport updatedAirport = airportService.update(airportId, airport);
        flightService.update(flightId, flight);

        return airportMapper.toDto(updatedAirport);
    }

    @Override
    public AirportDto deleteArrivingFlightAssignment(Long airportId, Long flightId) {
        Airport airport = airportService.findById(airportId)
                .orElseThrow(ResourceNotFoundException::new);
        Flight flight = flightService.findById(flightId)
                .orElseThrow(ResourceNotFoundException::new);

        airport.removeArrivingFlight(flight);
        flight.setArrivingAirport(null);

        Airport updatedAirport = airportService.update(airportId, airport);
        flightService.update(flightId, flight);

        return airportMapper.toDto(updatedAirport);
    }

    @Override
    public AirportDto addDepartingFlightAssignment(Long airportId, Long flightId) {
        Airport airport = airportService.findById(airportId)
                .orElseThrow(ResourceNotFoundException::new);
        Flight flight = flightService.findById(flightId)
                .orElseThrow(ResourceNotFoundException::new);

        try {
            FlightCreationAdviceDto advice = weatherApi.isSafeToCreateFlight(
                    airportId,
                    airportId,
                    flight.getDepartureTime(),
                    flight.getArrivalTime()
            );
            if (!advice.getResult()) {
                informAboutDangerousWeather(advice);
            }
        } catch (ApiException ex) {
            throw new ResourceNotFoundException(ex);
        }

        airport.addDepartingFlight(flight);
        flight.setDepartingAirport(airport);

        Airport updatedAirport = airportService.update(airportId, airport);
        flightService.update(flightId, flight);

        return airportMapper.toDto(updatedAirport);
    }

    @Override
    public AirportDto deleteDepartingFlightAssignment(Long airportId, Long flightId) {
        Airport airport = airportService.findById(airportId)
                .orElseThrow(ResourceNotFoundException::new);
        Flight flight = flightService.findById(flightId)
                .orElseThrow(ResourceNotFoundException::new);

        airport.removeDepartingFlight(flight);
        flight.setDepartingAirport(null);

        Airport updatedAirport = airportService.update(airportId, airport);
        flightService.update(flightId, flight);

        return airportMapper.toDto(updatedAirport);
    }

    @Override
    public AirportDto addCityAssignment(Long airportId, Long cityId) {
        Airport airport = airportService.findById(airportId)
                .orElseThrow(ResourceNotFoundException::new);
        City city = cityService.findById(cityId)
                .orElseThrow(ResourceNotFoundException::new);

        airport.setCity(city);
        city.addAirport(airport);

        Airport updatedAirport = airportService.update(airportId, airport);
        cityService.update(cityId, city);

        return airportMapper.toDto(updatedAirport);
    }

    @Override
    public AirportDto deleteCityAssignment(Long airportId, Long cityId) {
        Airport airport = airportService.findById(airportId)
                .orElseThrow(ResourceNotFoundException::new);
        City city = cityService.findById(cityId)
                .orElseThrow(ResourceNotFoundException::new);

        airport.setCity(null);
        city.removeAirport(airport);

        Airport updatedAirport = airportService.update(airportId, airport);
        cityService.update(cityId, city);

        return airportMapper.toDto(updatedAirport);
    }

    private void informAboutDangerousWeather(FlightCreationAdviceDto advice) {
        throw new DangerousWeatherException(
                "Dangerous weather: " +
                advice.getReason().getValue() +
                " exceeded safe bounds"
        );
    }
}
