package cz.muni.fi.pa165.user.server.rest;

import cz.muni.fi.pa165.user.server.api.UserApiDelegate;
import cz.muni.fi.pa165.user.server.facade.UserFacade;
import cz.muni.fi.pa165.user.server.model.ActionDto;
import cz.muni.fi.pa165.user.server.model.NewActionDto;
import cz.muni.fi.pa165.user.server.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController implements UserApiDelegate {

    private final UserFacade userFacade;

    @Autowired
    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Override
    public ResponseEntity<UserDto> getUserById(Long id) {
        return ResponseEntity.of(userFacade.findById(id));
    }

    @Override
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return ResponseEntity.ok(userFacade.getAllUsers());
    }

    @Override
    public ResponseEntity<Void> registerUserAction(NewActionDto newActivityDto) {
        var principal = (OAuth2IntrospectionAuthenticatedPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userFacade.registerUserAction(
                principal.getAttribute("sub"),
                newActivityDto.getUrl(),
                newActivityDto.getHttpMethod()
        );
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<ActionDto>> getUserActions(Long id) {
        return ResponseEntity.ok(userFacade.getActionsOfUser(id));
    }
}
