package cz.muni.fi.pa165.user.server.service;

import cz.muni.fi.pa165.user.server.data.domain.Action;
import cz.muni.fi.pa165.user.server.data.domain.User;
import cz.muni.fi.pa165.user.server.data.repository.ActionRepository;
import cz.muni.fi.pa165.user.server.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

import static com.fasterxml.jackson.databind.type.LogicalType.Collection;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final ActionRepository actionRepository;

    @Autowired
    public UserService(UserRepository userRepository, ActionRepository actionRepository) {
        this.userRepository = userRepository;
        this.actionRepository = actionRepository;
    }

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void registerUserAction(String externalIdentifier, String url, String httpMethod) {
        Optional<User> maybeUser = userRepository.findByExternalIndetifier(externalIdentifier);
        User user = maybeUser.orElseGet(() -> {
            var newUser = new User();
            newUser.setExternalIdentifier(externalIdentifier);
            newUser = userRepository.save(newUser);
            return newUser;
        });
        var action = new Action();
        action.setUrl(url);
        action.setHttpMethod(httpMethod);
        action.setUser(user);
        actionRepository.save(action);
    }

    public Iterable<Action> getActionsOfUser(Long id) {
        return userRepository.findById(id).map(User::getActions).orElse(Collections.EMPTY_LIST);
    }
}
