package cz.muni.fi.pa165.user.server.data.repository;

import cz.muni.fi.pa165.user.server.data.domain.Action;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionRepository  extends CrudRepository<Action, Long>  {

}
