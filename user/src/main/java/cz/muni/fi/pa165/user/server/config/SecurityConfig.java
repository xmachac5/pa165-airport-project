package cz.muni.fi.pa165.user.server.config;

import cz.muni.fi.pa165.user.client.Authorities;
import cz.muni.fi.pa165.user.client.UserServiceInterceptionConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@Import(UserServiceInterceptionConfigurer.class)
public class SecurityConfig {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(x -> x
                        // swagger:
                        .requestMatchers("/swagger-ui/**").permitAll()
                        .requestMatchers("/v3/api-docs/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/").permitAll()
                        .requestMatchers(HttpMethod.GET, "/swagger-ui.html").permitAll()

                        // actuator
                        .requestMatchers("/actuator/**").permitAll()

                        // used under the authority of the user using the original api
                        .requestMatchers("/api/users/action").authenticated()

                        // require audit authority for reading from this service
                        .requestMatchers(HttpMethod.GET, "/api/users").hasAuthority(Authorities.AUDITOR)
                        .requestMatchers(HttpMethod.GET, "/api/users/**").hasAuthority(Authorities.AUDITOR)

                        // default deny
                        .anyRequest().denyAll()
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken);
        return http.build();
    }
}
