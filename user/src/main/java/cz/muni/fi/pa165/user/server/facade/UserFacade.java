package cz.muni.fi.pa165.user.server.facade;

import cz.muni.fi.pa165.user.server.mapper.ActionMapper;
import cz.muni.fi.pa165.user.server.mapper.UserMapper;
import cz.muni.fi.pa165.user.server.model.ActionDto;
import cz.muni.fi.pa165.user.server.model.UserDto;
import cz.muni.fi.pa165.user.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
public class UserFacade {

    private final UserService userService;
    private final UserMapper userMapper;
    private ActionMapper actionMapper;

    @Autowired
    public UserFacade(UserService userService, UserMapper userMapper, ActionMapper actionMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.actionMapper = actionMapper;
    }

    public Optional<UserDto> findById(Long id) {
        return userService.findById(id).map(userMapper::toDto);
    }

    public List<UserDto> getAllUsers() {
        return StreamSupport.stream(userService.getAllUsers().spliterator(), false)
                .map(userMapper::toDto)
                .toList();
    }

    public void registerUserAction(String foreignIdentifier, String url, String httpMethod) {
        userService.registerUserAction(foreignIdentifier, url, httpMethod);
    }

    public List<ActionDto> getActionsOfUser(Long id) {
        return StreamSupport.stream(userService.getActionsOfUser(id).spliterator(), false)
                .map(actionMapper::toDto)
                .toList();
    }
}
