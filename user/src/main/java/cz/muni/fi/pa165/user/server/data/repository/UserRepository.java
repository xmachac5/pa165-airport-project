package cz.muni.fi.pa165.user.server.data.repository;

import cz.muni.fi.pa165.user.server.data.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("SELECT u FROM users u WHERE u.externalIdentifier = :externalIdentifier")
    Optional<User> findByExternalIndetifier(@Param("externalIdentifier") String externalIdentifier);
}
