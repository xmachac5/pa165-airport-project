package cz.muni.fi.pa165.user.server.mapper;

import cz.muni.fi.pa165.user.server.data.domain.Action;
import cz.muni.fi.pa165.user.server.model.ActionDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ActionMapper {
    Action toEntity(ActionDto actionDto);

    ActionDto toDto(Action user);
}