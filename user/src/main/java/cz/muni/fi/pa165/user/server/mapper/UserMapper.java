package cz.muni.fi.pa165.user.server.mapper;

import cz.muni.fi.pa165.user.server.data.domain.User;
import cz.muni.fi.pa165.user.server.model.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User toEntity(UserDto userDto);

    UserDto toDto(User user);
}