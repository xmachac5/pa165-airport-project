package cz.muni.fi.pa165.user.server.data.domain;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.OffsetDateTime;
import java.util.List;

// name is important - "user" and "system_user" are reserved and produce weird errors
@Entity(name = "users")
@Data
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Action> actions;

    public void addAction(Action action) {
        actions.add(action);
    }

    @Column(unique = true)
    private String externalIdentifier;
}
